package structs

type ReadingTime struct {
	CulturSite int `json:"cultur_site,omitempty"`
	Time       int `json:"time,omitempty"`
}

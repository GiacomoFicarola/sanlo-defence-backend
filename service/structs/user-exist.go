package structs

type UserExist struct {
	Exists bool `json:"exists"`
	User   User `json:"user,omitempty"`
}

package database

func (db *appdbimpl) GeneralStatsSavedPlaces() (int, error) {
	var i = 0
	err := db.c.Get(&i, "SELECT COALESCE(COUNT(*),0) FROM Game WHERE victory = true;")

	if err != nil {
		return i, err
	}

	return i, nil
}

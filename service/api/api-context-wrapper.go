package api

import (
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/reqcontext"
	"git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/apicommon"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"net/http"
)

// wrap parses the request and adds a reqcontext.RequestContext instance related to the request.
func (rt *_router) wrap(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reqUUID, err := uuid.NewV4()
		if err != nil {
			rt.baseLogger.WithError(err).Error("can't generate a request UUID")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		tctx, span := rt.tracer.Start(r.Context(), "context")
		defer span.End()

		r = r.Clone(tctx)
		span.SetAttributes(attribute.String("reqUUID", reqUUID.String()))
		span.SetAttributes(attribute.String("url", r.URL.String()))

		var ctx = reqcontext.RequestContext{
			ReqUUID: reqUUID,
		}

		// Parse app common headers
		ctx.AppInfo, err = apicommon.ParseAppInfoHeaders(r)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			w.WriteHeader(http.StatusBadRequest)
			rt.baseLogger.WithFields(logrus.Fields{
				"remote-ip": r.RemoteAddr,
				"url":       r.URL,
				"method":    r.Method,
				"headers":   r.Header,
			}).WithError(err).Warning("error parsing app info headers")
			return
		}

		// Create a request-specific logger
		ctx.Logger = rt.baseLogger.WithFields(logrus.Fields{
			"reqid":     ctx.ReqUUID.String(),
			"lang":      ctx.AppInfo.Lang,
			"version":   ctx.AppInfo.VersionString,
			"build":     ctx.AppInfo.Build,
			"platform":  ctx.AppInfo.Platform,
			"remote-ip": r.RemoteAddr,
		})

		r = ctx.AttachToRequest(r)

		// Call the next handler in chain (usually, the handler function for the path)
		fn(w, r)
	}
}

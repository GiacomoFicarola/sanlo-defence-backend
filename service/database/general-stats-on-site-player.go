package database

func (db *appdbimpl) GeneralStatsOnSitePlayer(timeWhere string) (int, error) {
	var i = 0
	err := db.c.Get(&i, `
		SELECT COALESCE(COUNT(DISTINCT u.identifier),0) AS num_users
		FROM User u
		    JOIN Game g ON u.identifier = g.user
		WHERE g.geolocalized = true AND `+timeWhere+` ;`)

	if err != nil {
		return i, err
	}

	return i, nil
}

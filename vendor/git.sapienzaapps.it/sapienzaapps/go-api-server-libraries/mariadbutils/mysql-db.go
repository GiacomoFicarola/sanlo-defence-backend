package mariadbutils

import (
	"fmt"
	"net/url"
)

// DSNFromURL transforms a URL into a data source name for MySQL driver, as MySQL driver does not support standard URLs
func DSNFromURL(url *url.URL) string {
	return fmt.Sprintf("%s@tcp(%s)%s?%s", url.User.String(), url.Host, url.Path, url.Query().Encode())
}

/*
Webapi is the executable for the main web server.
It builds a web server around APIs from `service/api`.
Webapi connects to external resources needed (database) and starts two web servers: the API web server, and the debug.
Everything is served via the API web server, except debug variables (/debug/vars) and profiler infos (pprof).

Usage:

	webapi [flags]

Flags and configurations are handled automatically by the code in `load-configuration.go`.

Return values (exit codes):

	0
		The program ended successfully (no errors, stopped by signal)

	> 0
		The program ended due to an error

Note that this program will update the schema of the database to the latest version available (embedded in the
executable during the build).
*/
package main

import (
	"context"
	"errors"
	"expvar"
	"fmt"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/apilogger"
	"git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/mariadbutils"
	"git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/metrics"
	_ "github.com/amacneil/dbmate/pkg/driver/mysql"
	"github.com/ardanlabs/conf"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/jmoiron/sqlx"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"net/http"
	_ "net/http/pprof" // #nosec G108
	"net/url"
	"os"
	"os/signal"
	"syscall"
)

// main is the program entry point. The only purpose of this function is to call run() and set the exit code if there is
// any error
func main() {
	if err := run(); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "error: ", err)
		os.Exit(1)
	}
}

// run executes the program. The body of this function performs the following steps:
// * reads the configuration
// * creates and configure the logger
// * connects to any external resources (like databases, authenticators, etc.)
// * creates an instance of the service/api package
// * starts a debug server
// * starts the principal web server (using the service/api.Router.Handler() for HTTP handlers)
// * waits for any termination event: SIGTERM signal (UNIX), non-recoverable server error, etc.
// * closes the principal web server
func run() error {
	// Load Configuration and defaults
	cfg, err := loadConfiguration()
	if err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			return nil
		}
		return err
	}

	// Init logging
	logger, err := apilogger.New(cfg.Log)
	if err != nil {
		return err
	}

	// Print the build version for our logs. Also expose it under /debug/vars in the debug HTTP handler.
	logger.Infof("application initializing, version %q (%s)", AppVersion, BuildDate)
	expvar.NewString("build").Set(AppVersion)
	expvar.NewString("build-date").Set(BuildDate)

	// Init tracing
	tracer, err := tracerProvider(cfg.Telemetry.Name, cfg.Telemetry.URL, logger)
	if err != nil {
		logger.WithError(err).Error("error creating OpenTelemetry provider")
		return fmt.Errorf("creating OpenTelemetry provider: %w", err)
	}

	// Parse database connection string
	dsn, err := url.Parse(cfg.DB.DSN)
	if err != nil {
		logger.WithError(err).Error("error parsing connection string")
		return fmt.Errorf("parsing connection string: %w", err)
	} else if cfg.DB.DSN == "" {
		logger.Error("empty connection string")
		return errors.New("empty connection string")
	}

	if !cfg.DB.NoMigrate {
		// Migrate database to the latest schema if necessary
		err = migrateDB(logger, dsn)
		if err != nil {
			logger.WithError(err).Error("error migrating database")
			return fmt.Errorf("migrating database: %w", err)
		}
	}

	// Start Database
	logger.Info("initializing database support")
	db, err := sqlx.Open("mysql", mariadbutils.DSNFromURL(dsn))
	if err != nil {
		logger.WithError(err).Error("error parsing connection string")
		return fmt.Errorf("parsing connection string: %w", err)
	}
	err = db.Ping()
	if err != nil {
		logger.WithError(err).Error("error connecting to DB")
		return fmt.Errorf("connecting to db: %w", err)
	}
	defer func() {
		logger.Debug("database stopping")
		_ = db.Close()
	}()

	// Create a new instance of database.AppDatabase for packages
	appdb, err := database.New(db)
	if err != nil {
		logger.WithError(err).Error("error creating AppDatabase")
		return fmt.Errorf("creating AppDatabase: %w", err)
	}

	// Start Debug web service in a separate goroutine
	//
	// /debug/pprof - Added to the default mux by importing the net/http/pprof package.
	// /debug/vars - Added to the default mux by importing the expvar package.

	logger.Info("initializing debugging support")
	go func() {
		logger.Infof("debug listening %s", cfg.Web.DebugHost)
		err := http.ListenAndServe(cfg.Web.DebugHost, http.DefaultServeMux) // nolint:gosec // debug endpoint
		logger.Infof("debug listener closed: %v", err)
	}()

	// Start (main) API server
	logger.Info("initializing API server")

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	// Use a buffered channel because the signal package requires it.
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	// Create the API router
	apirouter, err := api.New(api.Config{
		Logger:     logger,
		Database:   appdb,
		Tracer:     tracer,
		AssetsPath: cfg.Web.AssetsPath,
	})
	if err != nil {
		logger.WithError(err).Error("error creating the API server instance")
		return fmt.Errorf("creating the API server instance: %w", err)
	}
	router := apirouter.Handler()

	// Handle errors gracefully (HTTP 500)
	router = panicHandler(cfg, logger, router)

	// Wrap the server for telemetry
	router = otelhttp.NewHandler(router, "")

	// Setup internal routes middleware
	router = internalRoutesHandler(router)

	// Apply CORS policy
	router = applyCORSHandler(router)

	// Handle reverse proxy if instructed to do so
	if cfg.Web.BehindProxy {
		router = handlers.ProxyHeaders(router)
	}

	// Log combined log to stdout if instructed to do so
	if cfg.Log.CombinedToStdout {
		router = FilterAccessLog(router)
	}

	// Add generic/HTTP level metrics
	router = metrics.MeasureServer(router)

	// Create the API server
	apiserver := http.Server{
		Addr:              cfg.Web.APIHost,
		Handler:           router,
		ReadTimeout:       cfg.Web.ReadTimeout,
		ReadHeaderTimeout: cfg.Web.ReadTimeout,
		WriteTimeout:      cfg.Web.WriteTimeout,
	}

	// Start the service listening for requests in a separate goroutine
	go func() {
		logger.Infof("API listening on %s", apiserver.Addr)
		serverErrors <- apiserver.ListenAndServe()
		logger.Infof("stopping API server")
	}()

	// Waiting for shutdown signal or POSIX signals
	select {
	case err := <-serverErrors:
		// Non-recoverable server error
		return fmt.Errorf("server error: %w", err)

	case sig := <-shutdown:
		logger.Infof("signal %v received, start shutdown", sig)

		// Asking API server to shut down and load shed.
		err := apirouter.Close()
		if err != nil {
			logger.WithError(err).Warning("graceful shutdown of apirouter error")
		}

		// Give outstanding requests a deadline for completion.
		ctx, cancel := context.WithTimeout(context.Background(), cfg.Web.ShutdownTimeout)
		defer cancel()

		// Asking listener to shut down and load shed.
		err = apiserver.Shutdown(ctx)
		if err != nil {
			logger.WithError(err).Warning("error during graceful shutdown of HTTP server")
			err = apiserver.Close()
		}

		// Log the status of this shutdown.
		switch {
		case sig == syscall.SIGSTOP:
			return errors.New("integrity issue caused shutdown")
		case err != nil:
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}

#!/usr/bin/env bash
set -eo pipefail

MARIADB="mysql -h 127.0.0.1 -u root -proot db"
MIGRATION_DIR=db/migrations
MIGR_IDEMP_DIR=${MIGR_IDEMP_DIR:-/tmp/migr_idemp_test}

git fetch

# Check if migrations have been changed in this merge request
#LAST_COMMON_COMMIT=$(git merge-base origin/devel $CI_COMMIT_SHA)
#if [ "$(git diff --name-only $LAST_COMMON_COMMIT $CI_COMMIT_SHA -- $MIGRATION_DIR)" == "" ]; then
#	echo "No changes detected in $MIGRATION_DIR, skipping checks"
#	exit 0
#fi

rm -rf "$MIGR_IDEMP_DIR"
mkdir -p "$MIGR_IDEMP_DIR"

# Wait for DB and prepare
dbmate wait
dbmate drop
dbmate create

for MIGRATION in "$MIGRATION_DIR"/*.sql; do
  ID=$(echo "$MIGRATION" | sort | grep -Eo '[0-9]{14}');
  echo ""
  echo "******** [ $ID ]"

  # Isolate the migration file: the idea here is to launch the migration file twice: 1st is the expected one, then we
  # drop the schema_migrations table (so `dbmate` will think that the migration was not applied yet), and then launch
  # the 2nd run
  # Note: we need to isolate the migration file because dbmate applies ALL migrations when issuing `dbmate up`, instead
  # we need to execute only one migration at a time
  cp "$MIGRATION" "$MIGR_IDEMP_DIR"/

  echo "Checking migrate:up for idempotency for #$ID"
  dbmate --migrations-dir "$MIGR_IDEMP_DIR" up
  echo "drop table schema_migrations;" | $MARIADB
  # idempotency test for :up
  dbmate --migrations-dir "$MIGR_IDEMP_DIR" up

  echo ""
  echo "Checking migrate:down for idempotency for #$ID"
  dbmate --migrations-dir "$MIGR_IDEMP_DIR" down
  echo "INSERT INTO schema_migrations VALUES ($ID);" | $MARIADB
  # idempotency test for :down
  dbmate --migrations-dir "$MIGR_IDEMP_DIR" down

  # Cleaning up for next migration
  dbmate --migrations-dir "$MIGR_IDEMP_DIR" up > /dev/null
  echo "drop table schema_migrations;" | $MARIADB
  rm "$MIGR_IDEMP_DIR"/*.sql
done

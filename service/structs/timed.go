package structs

type Timed struct {
	TimeLapse       string `json:"time_lapse,omitempty"`
	OnlinePlayers   int    `json:"online_players,omitempty"`
	OnSitePlayers   int    `json:"on_site_players,omitempty"`
	DefeatedEnemies int    `json:"defeated_enemies,omitempty"`
	SavedPlaces     int    `json:"saved_places,omitempty"`
	InvadedPlaces   int    `json:"invaded_places,omitempty"`
}

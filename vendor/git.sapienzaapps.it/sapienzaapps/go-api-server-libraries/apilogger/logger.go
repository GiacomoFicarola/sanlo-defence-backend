package apilogger

import (
	"github.com/sirupsen/logrus"
	"os"
)

// LogSettings describes the configuration for the logging backend
type LogSettings struct {
	Level            string `conf:"default:warn"`
	MethodName       bool   `conf:"default:false"`
	JSON             bool   `conf:"default:false"`
	Destination      string `conf:"default:stderr"` // Possible values: stderr, stdout, file, TODO
	File             string `conf:"default:/tmp/debug.log"`
	CombinedToStdout bool   `conf:"default:true"`
}

// New istantiates a new logging backend
func New(cfg LogSettings) (logrus.FieldLogger, error) {
	// Init Logging
	logger := logrus.New()

	// Setting output
	switch {
	case cfg.Destination == "stdout":
		logger.SetOutput(os.Stdout)
	case cfg.Destination == "stderr":
		logger.SetOutput(os.Stderr)
	case cfg.Destination == "file":
		file, err := os.OpenFile(cfg.File, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0600)
		if err == nil {
			logger.SetOutput(file)
		} else {
			logger.SetOutput(os.Stderr)
			logger.WithError(err).Error("Can't open log file for writing, using stderr")
		}
	}

	// Set logging level
	switch cfg.Level {
	case "trace":
		logger.SetLevel(logrus.TraceLevel)
	case "debug":
		logger.SetLevel(logrus.DebugLevel)
	case "warn":
		logger.SetLevel(logrus.WarnLevel)
	case "error":
		logger.SetLevel(logrus.ErrorLevel)
	case "info":
		fallthrough
	default:
		logger.SetLevel(logrus.InfoLevel)
	}

	if cfg.MethodName {
		logger.SetReportCaller(true)
	}

	if cfg.JSON {
		logger.SetFormatter(&logrus.JSONFormatter{})
	}

	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	return logger.WithFields(logrus.Fields{
		"hostname": hostname,
	}), nil
}

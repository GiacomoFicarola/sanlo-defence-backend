package structs

type User struct {
	Nickname string `json:"nickname,omitempty" db:"nickname"`
	UserID   int    `json:"userID,omitempty" db:"identifier"`
	Age      string `json:"age,omitempty" db:"age"`
}

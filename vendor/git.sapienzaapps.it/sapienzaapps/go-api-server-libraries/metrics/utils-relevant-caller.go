package metrics

import (
	"runtime"
	"strings"
)

// relevantCaller searches the call stack for the first function outside of
// current package.
// The purpose of this function is to provide more helpful error messages.
// Taken from net/http Golang package
func relevantCaller() runtime.Frame {
	pc := make([]uintptr, 16)
	n := runtime.Callers(1, pc)
	frames := runtime.CallersFrames(pc[:n])
	var frame runtime.Frame
	for {
		frame, more := frames.Next()
		if !strings.HasPrefix(frame.Function, "git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/metrics.") {
			return frame
		}
		if !more {
			break
		}
	}
	return frame
}

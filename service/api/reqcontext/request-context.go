/*
Package reqcontext contains the request context. Each request will have its own instance of RequestContext filled by the
middleware code in the api-context-wrapper.go (parent package).

Each value here should be assumed valid only per request only, with some exceptions like the logger.
*/
package reqcontext

import (
	"context"
	"git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/apicommon"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
	"net/http"
)

type ContextKey string

const ctxKeyName ContextKey = "appctx"

// RequestContext is the context of the request, for request-dependent parameters
type RequestContext struct {
	// ReqUUID is the request unique ID
	ReqUUID uuid.UUID

	// AppInfo contains structured info from App headers (if any)
	AppInfo apicommon.AppInfo

	// Logger is a custom field logger for the request
	Logger logrus.FieldLogger

	// UserID is the user ID in case of authentication
	UserID uuid.UUID
}

func (appctx RequestContext) AttachToRequest(r *http.Request) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), ctxKeyName, appctx))
}

func FromRequest(r *http.Request) RequestContext {
	ctx, ok := r.Context().Value(ctxKeyName).(RequestContext)
	if !ok {
		panic("request context missing")
	}
	return ctx
}

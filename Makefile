# DO NOT MODIFY

# ################################
# Load configuration
SHELL := /bin/bash

# If there is a dependency proxy configured in GitLab, let's use it, otherwise fallback to docker.io
DOCKER_PREFIX=
ifneq (${CI_PIPELINE_ID},) # If we're using the GitLab CI/CD pipeline
DOCKER_PREFIX=git.sapienzaapps.it:443/lab/dependency_proxy/containers/
endif
export DOCKER_PREFIX

OPENAPI_CLI="${DOCKER_PREFIX}openapitools/openapi-generator-cli:v5.4.0"
IBM_OPENAPI_VALIDATOR="${DOCKER_PREFIX}jamescooke/openapi-validator:0.84.1"
SWAGGER_EDITOR="${DOCKER_PREFIX}swaggerapi/swagger-editor:v4.6.1"

# Common variables
GITORIGIN := $(shell git remote get-url origin | sed 's/.git$$//' | sed 's/git@git.sapienzaapps.it://' | sed 's$$https://.*git.sapienzaapps.it/$$$$')
GIT_PUSH_ORIGIN := https://ci-pipeline-push-automation:${AUTOMATION_CI_PASSWORD}@git.sapienzaapps.it/${GITORIGIN}
PROJECT_NAME := $(shell echo ${GITORIGIN} | cut -d '/' -f 2 | tr '[:upper:]' '[:lower:]')
GROUP_NAME := $(shell echo ${GITORIGIN} | cut -d '/' -f 1 | tr '[:upper:]' '[:lower:]')
VERSION := $(shell git fetch --unshallow >/dev/null 2>&1; git describe --all --long --dirty 2>/dev/null)
ifeq (${VERSION},)
VERSION := no-git-version
endif

DOCKER_IMAGE_PATH := registry.git.sapienzaapps.it/${GROUP_NAME}/${PROJECT_NAME}
ifneq (${CI_REGISTRY_IMAGE},) # If we're using the GitLab CI/CD pipeline
DOCKER_IMAGE_PATH := ${CI_REGISTRY_IMAGE}
endif

BUILD_ID := ${CI_PIPELINE_ID}
ifeq (${BUILD_ID},) # Fallback for local builds
BUILD_ID = 0
endif

BUILD_DATE := $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
REPO_HASH := $(shell git rev-parse --verify HEAD)
DOCKER_IMAGE_TAG := $(shell date -u +"%Y-%m-%d").${BUILD_ID}

GO_MODULE := $(shell cat go.mod | grep module | cut -f 2 -d ' ')

# ################################
# Targets

.PHONY: all
all: build

# Build/dev section
.PHONY: info
info:
	$(info ** Environment info **)
	$(info Project name: ${PROJECT_NAME})
	$(info Group name: ${GROUP_NAME})
	$(info Version: ${VERSION})
	$(info Docker image path: ${DOCKER_IMAGE_PATH})
	$(info Docker image tag: ${DOCKER_IMAGE_TAG})
	@:

.PHONY: env
env:
	$(info PROJECT_NAME="${PROJECT_NAME}")
	$(info GROUP_NAME="${GROUP_NAME}")
	$(info VERSION="${VERSION}")
	$(info DOCKER_IMAGE_PATH="${DOCKER_IMAGE_PATH}")
	$(info DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG}")
	@:

.PHONY: build
build:
	go generate -mod=vendor ./...
	/bin/bash -euo pipefail -c "cd cmd; for ex in \$$(ls); do pushd \$$ex; CGO_ENABLED=0 go build -mod=vendor -ldflags \"-extldflags \\\"-static\\\"\" -a -installsuffix cgo -o ../../\$$ex.exe .; popd; done"

.PHONY: docker
docker: info
	docker build \
		-t ${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG} \
		--build-arg DOCKER_PREFIX="${DOCKER_PREFIX}" \
		--build-arg PROJECT_NAME="${PROJECT_NAME}" \
		--build-arg GROUP_NAME="${GROUP_NAME}" \
		--build-arg APP_VERSION="${VERSION}" \
		--build-arg BUILD_DATE="${BUILD_DATE}" \
		--build-arg REPO_HASH="${REPO_HASH}" \
		.

.PHONY: push
push:
	git tag BUILD-${DOCKER_IMAGE_TAG}
	@git remote set-url origin ${GIT_PUSH_ORIGIN}
	git push --tags origin BUILD-${DOCKER_IMAGE_TAG}
	docker push ${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG}
	docker tag ${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG} ${DOCKER_IMAGE_PATH}:latest
	docker push ${DOCKER_IMAGE_PATH}:latest

.PHONY: up-deps
up-deps:
	docker compose -p ${PROJECT_NAME} \
		-f demo/docker-compose.yml \
		up

.PHONY: stop
stop:
	DOCKER_IMAGE=${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG} \
		docker compose -p ${PROJECT_NAME} \
		-f demo/docker-compose.yml \
		-f demo/docker-compose.cicd.yml \
		stop

.PHONY: logs
logs:
	DOCKER_IMAGE=${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG} \
		docker compose -p ${PROJECT_NAME} \
		-f demo/docker-compose.yml \
		-f demo/docker-compose.cicd.yml \
		logs > docker-compose.log

.PHONY: down
down:
	DOCKER_IMAGE=${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG} \
		docker compose -p ${PROJECT_NAME} \
		-f demo/docker-compose.yml \
		-f demo/docker-compose.cicd.yml \
		down

.PHONY: keycloak-export
keycloak-export:
	$(eval KEYCLOAK_ID := $(shell DOCKER_IMAGE=${DOCKER_IMAGE_PATH}:${DOCKER_IMAGE_TAG} \
		docker compose -p ${PROJECT_NAME} \
		-f demo/docker-compose.yml \
		-f demo/docker-compose.cicd.yml \
		ps -q keycloak))
	chmod a+w "$(shell pwd)/demo/keycloak/"
	docker stop "${KEYCLOAK_ID}"
	docker commit "${KEYCLOAK_ID}" tmp-keycloak-save
	docker run -it --rm \
		-v "$(shell pwd)/demo/keycloak/:/host_data/" \
		-e DB_VENDOR=h2 \
		tmp-keycloak-save \
		export --dir /host_data/ --users realm_file
	docker rmi tmp-keycloak-save
	docker start "${KEYCLOAK_ID}"

.PHONY: install-dev-deps
install-dev-deps:
	go install github.com/psampaz/go-mod-outdated@v0.9.0
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.53.3
	go install github.com/google/go-licenses@v1.6.0
	go install golang.org/x/tools/cmd/godoc@latest
	go install github.com/amacneil/dbmate/v2@v2.4.0
	@bash -c 'command -v go-licenses > /dev/null || echo -e "\n\n**** Please add ${HOME}/go/bin in the PATH variable."'

.PHONY: test
test:
	go-licenses check ./cmd/* --ignore git.sapienzaapps.it --ignore golang.org/x/sys/unix --ignore github.com/cespare/xxhash/v2 2>&1
	go clean -testcache
	go test ./... -mod=vendor
	golangci-lint run
	go list -mod=mod -u -m -json all | go-mod-outdated -update -direct

.PHONY: vuln-dep-graph
vuln-dep-graph:
# Requires https://github.com/lucasepe/modgv and trivy and jq
	trivy -q fs --format=json --ignore-unfixed . | jq -r ".[].Vulnerabilities | .[] | (.PkgName + \"@v\" + .InstalledVersion)" | xargs -n 1 -I % sh -c 'go mod graph | digraph focus % | modgv | dot -Tpng | display'

.PHONY: vuln-dep
vuln-dep:
	trivy -q fs --ignore-unfixed .

.PHONY: dep-graph
dep-graph:
	go mod graph | modgv | dot -Tpng -o /tmp/dependencies.png
	xdg-open /tmp/dependencies.png

.PHONY: dep-update
dep-update:
	go get -u ./...
	go mod tidy -compat=1.17
	go mod vendor

.PHONY: validate-api
validate-api:
	docker run --rm \
		--user $(shell id -u):$(shell id -g) \
		-v "$(shell pwd)/doc/:/local/" \
		${OPENAPI_CLI} validate -i /local/api.yaml --recommend
	docker run --rm \
		--user $(shell id -u):$(shell id -g) \
		-v "$(shell pwd)/doc/:/local/" \
		-v "$(shell pwd)/.spectral.js:/.spectral.js:ro" \
		${IBM_OPENAPI_VALIDATOR} -e -s --ruleset /.spectral.js /local/api.yaml

.PHONY: doc
doc:
	$(info Starting web UI at http://127.0.0.1:9000 , press CTRL-C to stop)
	@docker run --rm -it \
		-v "$(shell pwd)/doc/:/doc/:ro" \
		-p 9000:8080 \
		-e SWAGGER_FILE=/doc/api.yaml \
		${SWAGGER_EDITOR}

.PHONY: godoc
godoc:
	$(info Open http://127.0.0.1:6060/pkg/${GO_MODULE}/)
ifdef ($GOROOT,)
	godoc
else
	godoc -goroot /usr/share/go
endif

# TODO: (CI) Targets for k6

.PHONY: beta
beta:
ifeq ($(shell git status --porcelain),)
	git pull origin devel
	git checkout master
	git pull origin master
	git merge --ff origin/devel
	git push origin master
	git checkout devel
else
	$(error Please commit all files first)
endif

.PHONY: release
release:
ifeq ($(shell git status --porcelain),)
	@echo "Releasing *TEST* to production!!!"; \
	read -p "Are you sure (y/n)? " -n 1 -r; \
	echo ""; \
	if [[ $$REPLY =~ ^[Yy] ]]; \
	then \
		git fetch && \
		TAGNAME=$$(git describe --tags --match "BUILD-*" --abbrev=0) && \
		git tag $${TAGNAME/BUILD-/RELEASE-} && \
		git push --tags origin $${TAGNAME/BUILD-/RELEASE-} && \
		git checkout devel; \
		echo "Pipeline started. At the end of the pipeline you can Pull & Redeploy on Portainer"; \
	else \
		echo "Operation aborted!"; \
	fi
else
	$(error Please commit all files first)
endif

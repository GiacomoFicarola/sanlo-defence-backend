package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) DataCultureSite(language string) ([]structs.Place, error) {
	var places []structs.Place
	var err error

	if language == "ita" {
		err = db.c.Select(&places, `
			SELECT identifier, name, description, is_special, url_img, address, level_coordinates
			FROM  Place`)
	} else {
		err = db.c.Select(&places, `
			SELECT p.identifier, pl.name, pl.description, p.is_special, p.url_img, p.address, p.level_coordinates
			FROM  PlaceLocalization pl
			JOIN Place p ON pl.place = p.identifier
			JOIN Language l ON pl.language = l.identifier
			WHERE l.name = ?`, language)
	}

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return places, err
	}

	if len(places) == 0 {
		return places, ErrNotFound
	}
	return places, nil
}

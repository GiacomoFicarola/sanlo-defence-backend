package dbmate

import (
	"io/fs"
	"os"
)

// osFS is an fs.FS implementation that just passes on to os.Open
type osFS struct{}

func (c osFS) Open(name string) (fs.File, error) {
	return os.Open(name)
}

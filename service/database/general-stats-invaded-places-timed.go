package database

func (db *appdbimpl) GeneralStatsInvadedPlacesTimed(timeWhere string) (int, error) {
	var i = 0
	err := db.c.Get(&i, `SELECT COALESCE(COUNT(*),0) AS num_victories
		FROM Game g
		WHERE g.victory = false AND `+timeWhere+` ;`)

	if err != nil {
		return i, err
	}

	return i, nil
}

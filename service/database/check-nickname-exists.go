package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) CheckIfNicknameExists(nickname string, deviceID string) (structs.UserExist, error) {
	var User structs.UserExist
	User.User.UserID = -1
	var err error

	if nickname != "" {
		err = db.c.Get(&User.User, `SELECT identifier, nickname FROM User WHERE nickname = ?`, nickname)
	} else {
		err = db.c.Get(&User.User, `SELECT identifier, nickname FROM User WHERE device_ID = ?`, deviceID)
	}

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return User, err
	}

	return User, nil
}

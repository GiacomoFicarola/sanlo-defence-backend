package structs

type Tower struct {
	ID        int    `json:"id,omitempty" db:"id"` // tower identifier
	TowerName string `json:"name,omitempty" db:"name"`
	AvgDamage int    `json:"avg_damage,omitempty" db:"avg_damage"` // damage the tower does
	Count     int    `json:"count,omitempty" db:"count"`           // number of this tower
}

package database

func (db *appdbimpl) EvaluateTimedStrings() (int, string, string, error) {
	var numGames = 0
	var timingCheckCount = 0

	var timeNames [3]string
	timeNames[0] = "daily"
	timeNames[1] = "weekly"
	timeNames[2] = "monthly"

	var strArrayTimingCountQueries [3]string
	strArrayTimingCountQueries[0] = "SELECT COALESCE(COUNT(*),0) AS num_games FROM Game g WHERE DATE(g.end_time) = CURDATE();"
	strArrayTimingCountQueries[1] = "SELECT COALESCE(COUNT(*),0) AS num_games FROM Game g WHERE DATE(g.end_time) > CURDATE() - INTERVAL 7 DAY;"
	strArrayTimingCountQueries[2] = "SELECT COALESCE(COUNT(*),0) AS num_games FROM Game g WHERE YEAR(g.end_time) = YEAR(CURDATE()) AND MONTH(g.end_time) = MONTH(CURDATE());"

	var gamesWhereCondtionBasedOnTimings [3]string
	gamesWhereCondtionBasedOnTimings[0] = "DATE(g.end_time) = CURDATE()"
	gamesWhereCondtionBasedOnTimings[1] = "DATE(g.end_time) > CURDATE() - INTERVAL 7 DAY"
	gamesWhereCondtionBasedOnTimings[2] = "YEAR(g.end_time) = YEAR(CURDATE()) AND MONTH(g.end_time) = MONTH(CURDATE())"

	for timingCheckCount < len(strArrayTimingCountQueries) {
		err := db.c.Get(&numGames, strArrayTimingCountQueries[timingCheckCount])

		if err != nil {
			return numGames, timeNames[timingCheckCount], gamesWhereCondtionBasedOnTimings[timingCheckCount], err
		}

		if numGames >= 100 || timingCheckCount >= len(strArrayTimingCountQueries)-1 {
			break
		}
		timingCheckCount++
	}

	return numGames, timeNames[timingCheckCount], gamesWhereCondtionBasedOnTimings[timingCheckCount], nil
}

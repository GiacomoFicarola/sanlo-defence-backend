package main

import (
	"github.com/bombsimon/logrusr/v4"
	"github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.opentelemetry.io/otel/trace"
)

// From https://github.com/open-telemetry/opentelemetry-go/blob/main/example/jaeger/main.go
// tracerProvider returns an OpenTelemetry TracerProvider configured to use
// the Jaeger exporter that will send spans to the provided url. The returned
// TracerProvider will also use a Resource configured with all the information
// about the application.
func tracerProvider(appname string, url string, logger logrus.FieldLogger) (trace.Tracer, error) {
	if url == "" {
		return trace.NewNoopTracerProvider().Tracer(""), nil
	}
	// Create the Jaeger exporter
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, err
	}
	tp := tracesdk.NewTracerProvider(
		// Always be sure to batch in production.
		tracesdk.WithBatcher(exp),
		// Record information about this application in a Resource.
		tracesdk.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(appname),
			attribute.String("appversion", AppVersion),
			attribute.String("builddate", BuildDate),
		)),
	)

	otel.SetTracerProvider(tp)
	logr := logrusr.New(logger)
	otel.SetLogger(logr)
	return otel.Tracer(""), nil
}

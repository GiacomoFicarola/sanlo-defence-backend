-- migrate:up
CREATE TABLE IF NOT EXISTS Tower
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(128)
);

-- migrate:down
DROP TABLE IF EXISTS Tower;
package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) UserReadingTime(deviceID string, culturSite int, time int) error {
	var userID int
	err := db.c.Get(&userID, "SELECT identifier FROM User WHERE device_ID = ?", deviceID)

	if errors.Is(err, sql.ErrNoRows) {
		return ErrNotFound
	}

	if err != nil {
		return err
	}

	var viewsTime structs.ViewsTime
	err = db.c.Get(&viewsTime, "SELECT time, views FROM ReadInfo WHERE user = ? AND place = ?", userID, culturSite)

	if errors.Is(err, sql.ErrNoRows) {
		_, errInser := db.c.Exec(`INSERT INTO ReadInfo (user, place, views, time)VALUES (?, ?, ?, ?);`, userID, culturSite, 1, time)

		if errInser != nil {
			return err
		}

		return nil
	}

	viewsTime.Time += time
	viewsTime.Views++

	_, err = db.c.Exec("UPDATE ReadInfo SET time = ?, views = ? WHERE user = ? AND place = ?", viewsTime.Time, viewsTime.Views, userID, culturSite)

	if err != nil {
		return err
	}

	return nil
}

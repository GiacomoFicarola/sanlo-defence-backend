-- migrate:up
CREATE TABLE IF NOT EXISTS Spawned
(
    enemy INT,
    game INT,
    avg_damage INT,
    count INT,
    FOREIGN KEY (enemy) REFERENCES Enemy(identifier),
    FOREIGN KEY (game) REFERENCES Game(identifier)
);

-- migrate:down
DROP TABLE IF EXISTS Spawned;

package database

func (db *appdbimpl) GeneralStatsDefeatedEnemies() (int, error) {
	var i = 0
	err := db.c.Get(&i, "SELECT COALESCE(SUM(defeated_enemies),0) FROM Game;")

	if err != nil {
		return i, err
	}

	return i, nil
}

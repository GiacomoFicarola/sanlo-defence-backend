-- migrate:up
ALTER TABLE User
	ADD COLUMN age_tmp VARCHAR(64);

UPDATE User
SET age_tmp = CAST(age AS CHAR);

ALTER TABLE User
DROP COLUMN age,
CHANGE COLUMN age_tmp age VARCHAR(64);

-- migrate:down
ALTER TABLE User
	ADD COLUMN age_tmp INT;

UPDATE User
SET age_tmp = CAST(age AS SIGNED);

ALTER TABLE User
DROP COLUMN age,
CHANGE COLUMN age_tmp age INT;

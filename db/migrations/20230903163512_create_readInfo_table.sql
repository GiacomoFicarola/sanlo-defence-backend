-- migrate:up
CREATE TABLE IF NOT EXISTS ReadInfo
(
    user INT,
    place INT,
    views INT,
    time INT,
    FOREIGN KEY (user) REFERENCES User(identifier),
    FOREIGN KEY (place) REFERENCES Place(identifier)
);

-- migrate:down
DROP TABLE IF EXISTS ReadInfo;

package structs

type PlayerStats struct {
	Rank     int     `json:"rank,omitempty"`
	Nickname string  `json:"nickname,omitempty" db:"nickname"`
	Wins     int     `json:"wins,omitempty"`
	Losses   int     `json:"losses,omitempty"`
	Delta    float32 `json:"delta,omitempty"`
	Status   *Status `json:"status,omitempty"`
}

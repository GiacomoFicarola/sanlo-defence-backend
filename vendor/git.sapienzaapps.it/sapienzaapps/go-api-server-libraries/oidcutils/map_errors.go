package oidcutils

import (
	"errors"
	"strings"
)

var (
	ErrMalformedJWT                      = errors.New("oidc: malformed jwt")
	ErrFailedToUnmarshalClaims           = errors.New("oidc: failed to unmarshal claims")
	ErrFailedToObtainSourceFromClaimName = errors.New("oidc: failed to obtain source from claim name")
	ErrSourceDoesntExists                = errors.New("oidc: source does not exist")
	ErrDifferentProvider                 = errors.New("oidc: id token issued by a different provider")
	ErrDifferentAudience                 = errors.New("oidc: expected audience")
	ErrClientIDMissing                   = errors.New("oidc: invalid configuration, clientID must be provided or SkipClientIDCheck must be set")
	ErrTokenExpired                      = errors.New("oidc: token is expired")
	ErrTokenNotYetValid                  = errors.New("oidc: token not yet valid") // oidc: current time %v before the nbf (not before) time: %v
	ErrTokenNotSigned                    = errors.New("oidc: id token not signed")
	ErrMultipleSignature                 = errors.New("oidc: multiple signatures on id token not supported")
	ErrUnsupportedAlgorithm              = errors.New("oidc: id token signed with unsupported algorithm")
	ErrFailedToVerifySignature           = errors.New("failed to verify signature")
	ErrInternalErrorPayloadVerification  = errors.New("oidc: internal error, payload parsed did not match previous payload")
)

var allSupportedErrors = []error{
	ErrMalformedJWT,
	ErrFailedToUnmarshalClaims,
	ErrFailedToObtainSourceFromClaimName,
	ErrSourceDoesntExists,
	ErrDifferentProvider,
	ErrDifferentAudience,
	ErrClientIDMissing,
	ErrTokenExpired,
	ErrTokenNotYetValid,
	ErrTokenNotSigned,
	ErrMultipleSignature,
	ErrUnsupportedAlgorithm,
	ErrFailedToVerifySignature,
	ErrInternalErrorPayloadVerification,
}

// MapErrorV2 maps error of github.com/coreos/go-oidc to errors that can be passed to `errors.Is` to check which kind of
// error we received, and act accordingly.
// Supports coreos/go-oidc v2.2.1+incompatible
func MapErrorV2(err error) error {
	if err == nil {
		return nil
	}

	// Special case: this error text has too generic "static prefix", so we match using other parts of the error
	if strings.Contains(err.Error(), "before the nbf (not before) time") {
		return ErrTokenNotYetValid
	}

	for _, e := range allSupportedErrors {
		if strings.HasPrefix(err.Error(), e.Error()) {
			return e
		}
	}

	// If we can't recognize the error, return the same error
	return err
}

/*
Package db contains an embedded filesystem with all SQL migrations. It's used by tools like dbmate to update the
database structure.

Database migrations are embedded in the Go code to ease distribution of the binary software.
*/
package db

import "embed"

// Migrations is an embed.FS instance containing all SQL migrations from `migrations/` directory.
//
//go:embed "migrations/*"
var Migrations embed.FS

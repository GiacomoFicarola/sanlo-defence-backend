package api

import (
	"encoding/json"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"net/http"
)

func (rt *_router) userReadingTime(w http.ResponseWriter, r *http.Request) {
	if !validationmethods.IsValidUUID(r.Header.Get("deviceID")) {
		http.Error(w, "invalid deviceID value", http.StatusBadRequest)
		return
	}

	var readingTime structs.ReadingTime
	err := json.NewDecoder(r.Body).Decode(&readingTime)

	if err != nil {
		http.Error(w, "Invalid JSON Form", http.StatusBadRequest)
	}

	err = rt.db.UserReadingTime(r.Header.Get("deviceID"), readingTime.CulturSite, readingTime.Time)

	if errors.Is(err, database.ErrNotFound) {
		http.Error(w, "Culture_site not found", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

package structs

import "time"

type Game struct {
	Identifier      int       `json:"identifier,omitempty" db:"identifier"`
	StartTime       time.Time `json:"start_time,omitempty" db:"start_time"`             // Start date and time of the game
	EndTime         time.Time `json:"end_time,omitempty" db:"end_time"`                 // Date and time of the end game
	CultureSite     int       `json:"culture_site,omitempty" db:"place"`                // Identification of the place of culture
	Victory         bool      `json:"victory,omitempty" db:"victory"`                   // Identifies if the player has won or lost
	WaveReached     int       `json:"wave_reached,omitempty" db:"wave_reached"`         // Indicates the number of waves reached
	DefeatedEnemies int       `json:"defeated_enemies,omitempty" db:"defeated_enemies"` // number of enemies eliminated
	DamageDealt     int       `json:"damage_dealt,omitempty" db:"damage_dealt"`         // Indicates the damage inflicted
	DamageReceived  int       `json:"damage_received,omitempty" db:"damage_received"`   // Indicates the damage suffered
	Result          Status    `json:"result,omitempty" db:"result"`
	Geolocalized    bool      `json:"geolocalized,omitempty" db:"geolocalized"`       // The game was or was not held in San Lorenzo
	CountPowerUps   int       `json:"count_power_ups,omitempty" db:"count_power_ups"` // Number of times the power up has been used
	BuiltTowers     []Tower   `json:"built_towers,omitempty" db:"built_towers"`
	SpawnedEnemies  []Enemy   `json:"spawned_enemies,omitempty" db:"spawned_enemies"`
	HardMode        bool      `json:"hard_mode,omitempty" db:"hard_mode"` // Indicates the novice or expert mode
}

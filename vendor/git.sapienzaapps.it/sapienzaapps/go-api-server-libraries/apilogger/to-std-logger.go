package apilogger

import (
	"github.com/sirupsen/logrus"
	"log"
)

type logrusStdLoggerWriter struct {
	lr    logrus.FieldLogger
	level logrus.Level
}

func (l logrusStdLoggerWriter) Write(p []byte) (int, error) {
	switch l.level {
	case logrus.TraceLevel:
		l.lr.Debug(string(p))
	case logrus.DebugLevel:
		l.lr.Debug(string(p))
	case logrus.WarnLevel:
		l.lr.Warning(string(p))
	case logrus.ErrorLevel:
		l.lr.Error(string(p))
	case logrus.InfoLevel:
		fallthrough
	default:
		l.lr.Info(string(p))
	}
	return len(p), nil
}

func ToStdLogger(lr logrus.FieldLogger, level logrus.Level) *log.Logger {
	return log.New(logrusStdLoggerWriter{lr, level}, "", 0)
}

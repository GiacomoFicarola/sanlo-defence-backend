-- migrate:up
CREATE TABLE IF NOT EXISTS Place
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(256) NOT NULL,
    description VARCHAR(1024),
    is_special BOOLEAN,
    url_img VARCHAR(256),
    address VARCHAR(512),
    level_coordinates VARCHAR(30)
);

-- migrate:down
DROP TABLE IF EXISTS Place;

package main

import (
	"github.com/gorilla/handlers"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"runtime/debug"
)

// panicHandler registers handlers that are executed in case of panics() inside the router code. It should never happen,
// but it's better safe than sorry.
//
// When the API server is configured for debug logging, the stack trace is printed out. Otherwise, errors are logged in
// the logrus logger passed as parameter. In any case, the response will be set to HTTP 500 (if it wasn't sent already)
func panicHandler(cfg WebAPIConfiguration, logger logrus.FieldLogger, hdl http.Handler) http.Handler {
	// Debug/trace levels are used in test env / dev env. If debug is used, and we have a TTY on stdout, print the stack
	// trace in a human form
	if (cfg.Log.Level == "debug" || cfg.Log.Level == "trace") && isTTY(os.Stdout) {
		return handlers.RecoveryHandler(handlers.PrintRecoveryStack(true))(hdl)
	}

	// In higher log levels means that the app is in production for sure, so we log the stack trace as field
	rt, ok := hdl.(*httprouter.Router)
	if ok {
		rt.PanicHandler = func(w http.ResponseWriter, r *http.Request, err interface{}) {
			fields := logrus.Fields{
				"url":    r.URL,
				"method": r.Method,
				"stack":  string(debug.Stack()),
			}
			if err2, ok := err.(error); ok {
				logger.WithFields(fields).WithError(err2).Error("internal server error")
			} else {
				logger.WithFields(fields).Error("internal server error ", err)
			}
			w.WriteHeader(http.StatusInternalServerError)
		}
		return rt
	}

	// If the function is used in a router different from httprouter.Router, we don't know how to handle that, so just
	// return 500 and hope for the best.
	return handlers.RecoveryHandler(handlers.PrintRecoveryStack(false))(hdl)
}

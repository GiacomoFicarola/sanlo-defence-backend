package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) MostDefendedPlaces(language string) ([]structs.Place, error) {
	var places []structs.Place
	var err error
	if language == "ita" {
		err = db.c.Select(&places, `
		SELECT p.identifier, p.name, p.description, p.is_special,
			   p.url_img, p.address, p.level_coordinates, COUNT(g.identifier) AS num_games
		FROM Game g
		JOIN Place p ON g.place = p.identifier
		WHERE g.victory = true AND p.is_special = false
		GROUP BY g.place
		ORDER BY num_games DESC
		LIMIT 3;`)
	} else {
		err = db.c.Select(&places, `
		SELECT p.identifier, pl.name, pl.description, p.is_special,
			   p.url_img, p.address, p.level_coordinates, COUNT(g.identifier) AS num_games
		FROM Game g
		JOIN Place p ON g.place = p.identifier
		JOIN PlaceLocalization pl ON pl.place = p.identifier
		JOIN Language l ON pl.language = l.identifier
		WHERE g.victory = true AND l.name = ? AND p.is_special = false
		GROUP BY g.place
		ORDER BY num_games DESC
		LIMIT 3;`, language)
	}

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return places, err
	}

	if len(places) == 0 {
		return places, ErrNotFound
	}
	return places, nil
}

package main

import (
	"os"
)

// isTTY returns always false on Windows (not supported)
func isTTY(fp *os.File) bool {
	return false
}

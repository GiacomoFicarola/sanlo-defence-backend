package api

import (
	"encoding/json"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"net/http"
)

func (rt *_router) registerNewUser(w http.ResponseWriter, r *http.Request) {
	var newUser structs.UserReg
	var userID structs.UserID
	err := json.NewDecoder(r.Body).Decode(&newUser)

	if err != nil {
		http.Error(w, "Invalid JSON Form", http.StatusBadRequest)
	}

	if !validationmethods.IsValidUUID(newUser.DeviceID) {
		http.Error(w, "Invalid deviceID", http.StatusBadRequest)
		return
	}

	if !validationmethods.IsValidNickname(newUser.Nickname) {
		http.Error(w, "Invalid nickname", http.StatusBadRequest)
		return
	}

	userID.UserID, err = rt.db.RegisterNewUser(newUser.Nickname, newUser.DeviceID, newUser.Age)

	if err != nil && errors.Is(err, database.ErrDuplicate) {
		http.Error(w, "Nickname or deviceID already exist", http.StatusConflict)
		return
	}

	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(userID)
}

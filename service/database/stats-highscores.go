package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) StatsHighscores(userID int, time string, mode string) (structs.PlayersStats, error) {
	var playerstats structs.PlayersStats
	var err error
	var isHardMode = false
	switch mode {
	case "novice":
	default:
		isHardMode = true
	}

	switch time {
	case "allTime":
		err := db.c.Select(&playerstats.Ranks, `
				SELECT
    				userWL.nickname AS nickname,
    				userWL.wins AS wins,
    				userWL.losses AS losses,
    				userWL.wins - userWL.losses AS delta,
    				RANK() OVER (ORDER BY userWL.wins - userWL.losses DESC) AS rank,
					CASE
						WHEN (userWL.wins - userWL.losses) >= 100 THEN 'Exceptional'
						WHEN (userWL.wins - userWL.losses) BETWEEN 50 AND 99 THEN 'Excellent'
						WHEN (userWL.wins - userWL.losses) BETWEEN 30 AND 49 THEN 'Optimal'
						WHEN (userWL.wins - userWL.losses) BETWEEN 20 AND 29 THEN 'Good'
						WHEN (userWL.wins - userWL.losses) BETWEEN 10 AND 19 THEN 'Sufficient'
						ELSE 'Insufficient'
					END AS status
				FROM (
					SELECT
						u.nickname AS nickname,
						COUNT(CASE WHEN g.victory = true THEN 1 END) AS wins,
						COUNT(CASE WHEN g.victory = false THEN 1 END) AS losses
					FROM
						User u
					JOIN Game g ON u.identifier = g.user
					WHERE
						g.hard_mode = ? AND u.deleted = false
					GROUP BY
						u.identifier, u.nickname
				) AS userWL
				ORDER BY delta DESC, wins DESC
				LIMIT 100;`, isHardMode)

		if err != nil && errors.Is(err, sql.ErrNoRows) {
			err = nil
		}

		if err != nil {
			return playerstats, err
		}

		err = db.c.Get(&playerstats.PlayerRank, `
				SELECT
				    rankings.nickname AS nickname,
    				rankings.wins AS wins,
    				rankings.losses AS losses,
    				rankings.wins - rankings.losses AS delta,
    				rankings.rank AS rank
				FROM (
					SELECT
					    userWL.userID AS userID,
						userWL.nickname AS nickname,
						userWL.wins AS wins,
						userWL.losses AS losses,
						userWL.wins - userWL.losses AS delta,
						RANK() OVER (ORDER BY userWL.wins - userWL.losses DESC) AS rank,
						CASE
							WHEN (userWL.wins - userWL.losses) >= 100 THEN 'Exceptional'
							WHEN (userWL.wins - userWL.losses) BETWEEN 50 AND 99 THEN 'Excellent'
							WHEN (userWL.wins - userWL.losses) BETWEEN 30 AND 49 THEN 'Optimal'
							WHEN (userWL.wins - userWL.losses) BETWEEN 20 AND 29 THEN 'Good'
							WHEN (userWL.wins - userWL.losses) BETWEEN 10 AND 19 THEN 'Sufficient'
							ELSE 'Insufficient'
						END AS status
					FROM (
						SELECT
							u.nickname AS nickname, u.identifier AS userID,
							COUNT(CASE WHEN g.victory = true THEN 1 END) AS wins,
							COUNT(CASE WHEN g.victory = false THEN 1 END) AS losses
						FROM
							User u
						JOIN Game g ON u.identifier = g.user
						WHERE
							g.hard_mode = ? AND u.deleted = false
						GROUP BY
							u.identifier, u.nickname
					) AS userWL
					ORDER BY delta DESC, wins DESC
				) AS rankings
				WHERE rankings.userID = ?
				;`, isHardMode, userID)

		if err != nil && errors.Is(err, sql.ErrNoRows) {
			err = nil
			playerstats.PlayerRank.Rank = -1
		}

		playerstats.Time = "allTime"

		if err != nil {
			return playerstats, err
		}
	default:
		err = calculateBasedOnGames(isHardMode, db, &playerstats, userID)
		if err != nil {
			return playerstats, ErrNotFound
		}
	}
	return playerstats, nil
}

func calculateBasedOnGames(isHardMode bool, db *appdbimpl, playerstats *structs.PlayersStats, userID int) error {
	var numGames = 0
	var timingCheckCount = 0

	var strArrayTimingCountQueries [2]string
	strArrayTimingCountQueries[0] = "SELECT COALESCE(COUNT(*),0) AS num_games FROM Game g JOIN User u ON u.identifier = g.user WHERE g.hard_mode = ? AND u.deleted = false AND DATE(g.end_time) = CURDATE();"
	strArrayTimingCountQueries[1] = "SELECT COALESCE(COUNT(*),0) AS num_games FROM Game g JOIN User u ON u.identifier = g.user WHERE g.hard_mode = ? AND u.deleted = false AND DATE(g.end_time) > CURDATE() - INTERVAL 7 DAY;"

	for timingCheckCount < len(strArrayTimingCountQueries) {
		err := db.c.Get(&numGames, strArrayTimingCountQueries[timingCheckCount], isHardMode)

		if err != nil {
			return err
		}

		if numGames < 100 {
			timingCheckCount++
		} else {
			break
		}
	}

	var gamesWhereCondtionBasedOnTimings [3]string

	gamesWhereCondtionBasedOnTimings[0] = "AND DATE(g.end_time) = CURDATE()"
	gamesWhereCondtionBasedOnTimings[1] = "AND DATE(g.end_time) > CURDATE() - INTERVAL 7 DAY"
	gamesWhereCondtionBasedOnTimings[2] = "AND YEAR(g.end_time) = YEAR(CURDATE()) AND MONTH(g.end_time) = MONTH(CURDATE())"

	stmt := `
		SELECT
    		userWL.nickname AS nickname,
    		userWL.wins AS wins,
    		userWL.losses AS losses,
    		userWL.wins - userWL.losses AS delta,
			RANK() OVER (ORDER BY userWL.wins - userWL.losses DESC) AS rank,
			CASE
				WHEN (userWL.wins - userWL.losses) >= 100 THEN 'Exceptional'
				WHEN (userWL.wins - userWL.losses) BETWEEN 50 AND 99 THEN 'Excellent'
				WHEN (userWL.wins - userWL.losses) BETWEEN 30 AND 49 THEN 'Optimal'
				WHEN (userWL.wins - userWL.losses) BETWEEN 20 AND 29 THEN 'Good'
				WHEN (userWL.wins - userWL.losses) BETWEEN 10 AND 19 THEN 'Sufficient'
				ELSE 'Insufficient'
			END AS status
		FROM (
			SELECT
				u.nickname AS nickname,
				COUNT(CASE WHEN g.victory = true THEN 1 END) AS wins,
				COUNT(CASE WHEN g.victory = false THEN 1 END) AS losses
			FROM
				User u
			JOIN Game g ON u.identifier = g.user
			WHERE
				g.hard_mode = ? AND u.deleted = false ` + gamesWhereCondtionBasedOnTimings[timingCheckCount] + `
			GROUP BY
				u.identifier, u.nickname) AS userWL
		ORDER BY delta DESC , wins DESC LIMIT 100;`

	err := db.c.Select(&playerstats.Ranks, stmt, isHardMode)

	if err != nil && errors.Is(err, sql.ErrNoRows) {
		err = nil
	}

	if err != nil {
		return err
	}

	err = db.c.Get(&playerstats.PlayerRank, `
				SELECT
				    rankings.nickname AS nickname,
    				rankings.wins AS wins,
    				rankings.losses AS losses,
    				rankings.wins - rankings.losses AS delta,
    				rankings.rank AS rank
				FROM (
					SELECT
					    userWL.userID AS userID,
						userWL.nickname AS nickname,
						userWL.wins AS wins,
						userWL.losses AS losses,
						userWL.wins - userWL.losses AS delta,
						RANK() OVER (ORDER BY userWL.wins - userWL.losses DESC) AS rank,
						CASE
							WHEN (userWL.wins - userWL.losses) >= 100 THEN 'Exceptional'
							WHEN (userWL.wins - userWL.losses) BETWEEN 50 AND 99 THEN 'Excellent'
							WHEN (userWL.wins - userWL.losses) BETWEEN 30 AND 49 THEN 'Optimal'
							WHEN (userWL.wins - userWL.losses) BETWEEN 20 AND 29 THEN 'Good'
							WHEN (userWL.wins - userWL.losses) BETWEEN 10 AND 19 THEN 'Sufficient'
							ELSE 'Insufficient'
						END AS status
					FROM (
						SELECT
							u.nickname AS nickname, u.identifier AS userID,
							COUNT(CASE WHEN g.victory = true THEN 1 END) AS wins,
							COUNT(CASE WHEN g.victory = false THEN 1 END) AS losses
						FROM
							User u
						JOIN Game g ON u.identifier = g.user
						WHERE
							g.hard_mode = ? AND u.deleted = false `+gamesWhereCondtionBasedOnTimings[timingCheckCount]+`
						GROUP BY
							u.identifier, u.nickname
					) AS userWL
					ORDER BY delta DESC, wins DESC
				) AS rankings
				WHERE rankings.userID = ?
				;`, isHardMode, userID)

	if err != nil && errors.Is(err, sql.ErrNoRows) {
		err = nil
		playerstats.PlayerRank.Rank = -1
	}

	switch timingCheckCount {
	case 0:
		playerstats.Time = "daily"
	case 1:
		playerstats.Time = "weekly"
	default:
		playerstats.Time = "monthly"
	}

	return err
}

package structs

type Enemy struct {
	ID        int    `json:"id,omitempty" db:"id"`                 // enemy identifier
	Name      string `json:"name,omitempty" db:"name"`             // enemy name
	AvgDamage int    `json:"avg_damage,omitempty" db:"avg_damage"` // damage done by the enemy on average
	Count     int    `json:"count,omitempty" db:"count"`           // number of this enemy
}

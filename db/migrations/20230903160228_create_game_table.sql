-- migrate:up
CREATE TABLE IF NOT EXISTS Game
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    start_time DATETIME,
    end_time DATETIME,
    hard_mode BOOLEAN,
    victory BOOLEAN,
    wave_reached INT,
    placed_towers INT,
    defeated_enemies INT,
    damage_dealt INT,
    damage_received INT,
    result ENUM('Exceptional','Excellent','Optimal','Good','Sufficient','Insufficient') NOT NULL,
    geolocalized BOOLEAN,
    count_power_ups INT,
    place INT,
    user INT,
    FOREIGN KEY (place) REFERENCES Place(identifier),
    FOREIGN KEY (user) REFERENCES User(identifier)
);

-- migrate:down
DROP TABLE IF EXISTS Game;

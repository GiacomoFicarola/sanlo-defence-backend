package api

import (
	"encoding/json"
	"errors"
	validationmethods "git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"net/http"
)

func (rt *_router) mostDefendedPlace(w http.ResponseWriter, r *http.Request) {
	var err error
	if !validationmethods.IsValidLanguage(r.URL.Query().Get("language")) {
		http.Error(w, "Invalid language value", http.StatusBadRequest)
		return
	}

	places, err := rt.db.MostDefendedPlaces(r.URL.Query().Get("language"))

	if errors.Is(err, database.ErrNotFound) {
		places = rt.db.MostDefendedPlacesEmpty(r.URL.Query().Get("language"))
		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(places)
		return
	}

	if err != nil {
		http.Error(w, "Can't execute", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(places)
}

package structs

type UserReg struct {
	Nickname string `json:"nickname,omitempty"`
	DeviceID string `json:"deviceID,omitempty"`
	Age      string `json:"age,omitempty"`
}

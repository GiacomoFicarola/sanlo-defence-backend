-- migrate:up
CREATE TABLE IF NOT EXISTS Enemy
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(128)
);

-- migrate:down
DROP TABLE IF EXISTS Enemy;
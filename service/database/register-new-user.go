package database

func (db *appdbimpl) RegisterNewUser(nickname string, deviceID string, age string) (int, error) {
	user, err := db.CheckIfNicknameExists(nickname, deviceID)
	var errInser error
	var userID int

	if err != nil {
		return -1, err
	}

	if user.Exists {
		return -1, ErrDuplicate
	}

	_, errInser = db.c.Exec(" INSERT INTO User (nickname, device_ID, age, deleted) VALUES (?, ?, ?, false)",
		nickname, deviceID, age)

	if errInser != nil {
		return -1, err
	}

	err = db.c.Get(&userID, "SELECT identifier FROM User WHERE nickname = ?", nickname)

	if err != nil {
		return -1, err
	}
	return userID, nil
}

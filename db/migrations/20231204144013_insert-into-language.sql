-- migrate:up
CREATE PROCEDURE IF NOT EXISTS insertlanguageifnotexists(IN language VARCHAR(4))
BEGIN
    DECLARE language_count INT;

SELECT COUNT(*) INTO language_count FROM Language WHERE name = language;

IF language_count = 0 THEN
        INSERT INTO Language (name) VALUES (language);
END IF;
END;

CALL insertlanguageifnotexists('ita');
CALL insertlanguageifnotexists('eng');

-- migrate:down
CREATE PROCEDURE IF NOT EXISTS removelanguageifexists(IN language VARCHAR(4))
BEGIN
    IF EXISTS (SELECT * FROM Language WHERE name = language) THEN
DELETE FROM Language WHERE name = language;
END IF;
END;

CALL removelanguageifexists('ita');
CALL removelanguageifexists('eng');

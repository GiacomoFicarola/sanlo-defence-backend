package metrics

import (
	"bufio"
	"errors"
	"github.com/sirupsen/logrus"
	"net"
	"net/http"
	"path"
)

// StatusCollectorWriter is a Response Writer which is able to store the HTTP
// status code
type StatusCollectorWriter struct {
	http.ResponseWriter
	status int
	length int
	logger logrus.FieldLogger
}

// WriteHeader sends an HTTP response header with the provided
// status code.
//
// If WriteHeader is not called explicitly, the first call to Write
// will trigger an implicit WriteHeader(http.StatusOK).
// Thus explicit calls to WriteHeader are mainly used to
// send error codes.
//
// The provided code must be a valid HTTP 1xx-5xx status code.
// Only one header may be written. Go does not currently
// support sending user-defined 1xx informational headers,
// with the exception of 100-continue response header that the
// Server sends automatically when the Request.Body is read.
func (w *StatusCollectorWriter) WriteHeader(status int) {
	if w.status != 0 {
		if w.logger != nil {
			caller := relevantCaller()
			w.logger.Warnf("http: superfluous response.WriteHeader call from %s (%s:%d)", caller.Function, path.Base(caller.File), caller.Line)
		}
		return
	}
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *StatusCollectorWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	n, err := w.ResponseWriter.Write(b)
	w.length += n
	return n, err
}

func (w *StatusCollectorWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	h, ok := w.ResponseWriter.(http.Hijacker)
	if !ok {
		return nil, nil, errors.New("hijack not supported")
	}
	return h.Hijack()
}

//go:build aix || darwin || dragonfly || freebsd || linux || netbsd || openbsd || solaris

package main

import (
	"golang.org/x/sys/unix"
	"os"
)

// isTTY returns true when the file `fp` is attached to a TTY. Usually this function is called on `os.Stdout`.
func isTTY(fp *os.File) bool {
	_, err := unix.IoctlGetWinsize(int(fp.Fd()), unix.TIOCGWINSZ)
	return err == nil
}

package structs

type Status string

// List of status
const (
	EXCEPTIONAL  Status = "Exceptional"
	EXCELLENT    Status = "Excellent"
	OPTIMAL      Status = "Optimal"
	GOOD         Status = "Good"
	SUFFICIENT   Status = "Sufficient"
	INSUFFICIENT Status = "Insufficient"
)

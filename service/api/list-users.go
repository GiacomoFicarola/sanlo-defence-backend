package api

import (
	"encoding/json"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"net/http"
)

func (rt *_router) listUsers(w http.ResponseWriter, _ *http.Request) {
	users, err := rt.db.ListUsers()

	if errors.Is(err, database.ErrNotFound) {
		http.Error(w, "There aren't any user", http.StatusNotFound)
		return
	}

	if err != nil || len(users) == 0 {
		http.Error(w, "Can't execute", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(users)
}

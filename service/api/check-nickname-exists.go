package api

import (
	"encoding/json"
	validationmethods "git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"net/http"
)

func (rt *_router) checkNicknameExists(w http.ResponseWriter, r *http.Request) {
	nickname := r.URL.Query().Get("nickname")
	deviceID := r.URL.Query().Get("deviceID")

	if !validationmethods.IsValidNickname(nickname) && !validationmethods.IsValidUUID(deviceID) {
		http.Error(w, "Invalid nickname or deviceID value", http.StatusBadRequest)
		return
	}

	user, err := rt.db.CheckIfNicknameExists(nickname, deviceID)

	if user.User.UserID >= 0 {
		user.Exists = true
	} else {
		user.Exists = false
	}

	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(user)
}

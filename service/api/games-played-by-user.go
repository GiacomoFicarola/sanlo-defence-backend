package api

import (
	"encoding/json"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"net/http"
	"strconv"
)

func (rt *_router) gamesPlayedByUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.URL.Query().Get("userID"))
	if err != nil {
		http.Error(w, "Invalid userID value", http.StatusBadRequest)
		return
	}

	var games []structs.Game
	games, err = rt.db.GamesPlayedByTheUser(userID)
	if err != nil {
		http.Error(w, "Can't find games", http.StatusInternalServerError)
		return
	}

	var result structs.Games
	result.Games = games

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(result)
}

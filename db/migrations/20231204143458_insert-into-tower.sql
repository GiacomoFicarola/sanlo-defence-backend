-- migrate:up
CREATE PROCEDURE IF NOT EXISTS inserttowerifnotexists(IN tower_name VARCHAR(128))
BEGIN
    DECLARE tower_count INT;

SELECT COUNT(*) INTO tower_count FROM Tower WHERE name = tower_name;

IF tower_count = 0 THEN
        INSERT INTO Tower (name) VALUES (tower_name);
END IF;
END;

CALL inserttowerifnotexists('studente');
CALL inserttowerifnotexists('street artist');
CALL inserttowerifnotexists('professore');
CALL inserttowerifnotexists('marmista');
CALL inserttowerifnotexists('fioraia');
CALL inserttowerifnotexists('vecchine');

-- migrate:down
CREATE PROCEDURE IF NOT EXISTS removetowerifexists(IN tower_name VARCHAR(128))
BEGIN
    IF EXISTS (SELECT * FROM Tower WHERE name = tower_name) THEN
DELETE FROM Tower WHERE name = tower_name;
END IF;
END;

CALL removetowerifexists('studente');
CALL removetowerifexists('street artist');
CALL removetowerifexists('professore');
CALL removetowerifexists('marmista');
CALL removetowerifexists('fioraia');
CALL removetowerifexists('vecchine');

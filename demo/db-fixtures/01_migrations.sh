#!/usr/bin/env bash

export DATABASE_URL="mysql://root:root@/$MYSQL_DATABASE?socket=/run/mysqld/mysqld.sock&parseTime=true&loc=Europe%2fRome&time_zone=%27Europe%2FRome%27"
export DBMATE_MIGRATIONS_DIR=/migrations/
export DBMATE_NO_DUMP_SCHEMA=true

if compgen -G "/migrations/*.sql" > /dev/null; then
	dbmate up
else
	echo "!!!!!! NO MIGRATIONS FOUND !!!!!!"
fi


const ibmCloudValidationRules = require('@ibm-cloud/openapi-ruleset');
const { operationIdCaseConvention } = require('@ibm-cloud/openapi-ruleset/src/functions');
const { schemas } = require('@ibm-cloud/openapi-ruleset/src/collections');
const { truthy } = require('@stoplight/spectral-functions');

module.exports = {
    extends: ibmCloudValidationRules,
    rules: {
        'operation-id-case-convention': {
                description: 'Operation ID must follow camel case',
                message: '{{error}}',
                resolved: true,
                given: schemas,
                severity: 'error',
                then: {
                function: operationIdCaseConvention,
                functionOptions: {
                    type: 'camel'
                }
            }
        },
        'property-case-convention': 'off',
        'array-responses': 'off',
        'request-body-object': 'off',
        'enum-case-convention': 'off',
        'parameter-case-convention': 'off',
        'path-segment-case-convention': 'off',
        'string-boundary': 'error',
        'path-keys-no-trailing-slash': 'off',
        'request-body-name': 'off',
        'major-version-in-path': 'off',

		'content-entry-contains-schema': 'error',
		'content-entry-provided': {
			description:
				'Request bodies and non-204 responses should define a content object',
			given: [
				"$.paths[*][get,post,put,patch,delete].responses[?(@property != '204' && @property != '202' && @property != '101' && @property < 300)]",
				'$.paths[*][*].requestBody'
			],
			severity: 'error',
			// formats: [oas3],
			resolved: true,
			then: {
				field: 'content',
				function: truthy
			}
		},

		'inline-response-schema': 'off',
		'operation-id-naming-convention': 'off',
		'operation-tag-defined': 'off',
		'authorization-parameter': 'error',
		'content-type-parameter': 'warn',
		'description-mentions-json': 'warn',
		'duplicate-path-parameter': 'error',
		'ibm-content-type-is-specific': 'warn',
		'ibm-error-content-type-is-json': 'off',
		'response-error-response-schema': 'off',
		'ibm-sdk-operations': 'off',
		'missing-required-property': 'error',
		'operation-summary': 'warn',
		'pagination-style': 'off',
		'parameter-default': 'off',
		'parameter-description': 'off',
		'parameter-order': 'off',
		'parameter-schema-or-content': 'error',
		'prohibit-summary-sentence-style': 'off',
		'property-case-collision': 'error',
		'property-description': 'warn',
		'property-inconsistent-name-and-type': 'warn',
		'response-example-provided': 'off',
		'schema-description': 'warn',
		'security-schemes': 'error',
		'server-variable-default-value': 'off',
		'unused-tag': 'warn',
		'valid-type-format': 'off',
		'oas3-api-servers': false,
	}
};

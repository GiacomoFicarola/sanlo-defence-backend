package api

import (
	"encoding/json"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"net/http"
	"time"
)

func (rt *_router) registerNewGame(w http.ResponseWriter, r *http.Request) {
	if !validationmethods.IsValidUUID(r.Header.Get("deviceID")) {
		http.Error(w, "Invalid deviceID value", http.StatusBadRequest)
		return
	}

	var game structs.Game
	err := json.NewDecoder(r.Body).Decode(&game)
	game.EndTime = time.Now()

	if err != nil {
		http.Error(w, "Invalid game value parsing", http.StatusBadRequest)
		return
	}

	if game.StartTime.After(time.Now()) {
		http.Error(w, "Invalid time value", http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	err = rt.db.RegisterNewGame(r.Header.Get("deviceID"), game)

	if errors.Is(err, database.ErrNotFound) {
		http.Error(w, "Can't found deviceID", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, "Can't insert the game", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

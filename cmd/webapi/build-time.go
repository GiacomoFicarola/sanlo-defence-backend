package main

// These variables are modified at build-time. Defaults here should support "go run" in development workstations

// AppVersion contains the app version (tag + commit after tag + current git ref)
var AppVersion = "devel"

// BuildDate contains the timestamp of the build
var BuildDate = "n/a"

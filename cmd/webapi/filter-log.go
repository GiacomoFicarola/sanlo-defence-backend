package main

import (
	"github.com/gorilla/handlers"
	"net/http"
	"os"
)

// FilterAccessLog removes the "access log" entries for internal endpoints like metrics or liveness
func FilterAccessLog(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI == "/metrics" ||
			r.RequestURI == "/version" ||
			r.RequestURI == "/liveness" {
			// Bypass for internal endpoints
			h.ServeHTTP(w, r)
		} else {
			handlers.CombinedLoggingHandler(os.Stdout, h).ServeHTTP(w, r)
		}
	})
}

/*
Package database is the middleware between the app database and the code. All data (de)serialization (save/load) from a
persistent database are handled here. Database specific logic should never escape this package.

To use this package you need to apply migrations to the database if needed/wanted, connect to it (using the database
data source name from config), and then initialize an instance of AppDatabase from the DB connection.

For example, this code adds a parameter in `webapi` executable for the database data source name (add it to the
main.WebAPIConfiguration structure):

	DB struct {
		DSN string `conf:""`
	}

This is an example on how to migrate the DB and connect to it:

	// Parse database connection string
	dsn, err := url.Parse(cfg.DB.DSN)
	if err != nil {
		logger.WithError(err).Error("error parsing connection string")
		return fmt.Errorf("parsing connection string: %w", err)
	} else if cfg.DB.DSN == "" {
		logger.Error("empty connection string")
		return errors.New("empty connection string")
	}

	// Migrate database to the latest schema if necessary
	err = migrateDB(logger, dsn)
	if err != nil {
		logger.WithError(err).Error("error migrating database")
		return fmt.Errorf("migrating database: %w", err)
	}

	// Start Database
	logger.Println("initializing database support")
	db, err := sqlx.Open("mysql", mariadbutils.DSNFromURL(dsn))
	if err != nil {
		logger.WithError(err).Error("error parsing connection string")
		return fmt.Errorf("parsing connection string: %w", err)
	}
	err = db.Ping()
	if err != nil {
		logger.WithError(err).Error("error connecting to DB")
		return fmt.Errorf("connecting to db: %w", err)
	}
	defer func() {
		logger.Debug("database stopping")
		_ = db.Close()
	}()

Then you can initialize the AppDatabase and pass it to the api package.
*/
package database

import (
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"github.com/jmoiron/sqlx"
)

// AppDatabase is the high level interface for the DB
type AppDatabase interface {
	CheckIfNicknameExists(nickname string, deviceID string) (structs.UserExist, error)
	UserDelete(deviceID string) error
	ListUsers() ([]structs.User, error)
	DataCultureSite(language string) ([]structs.Place, error)
	MostDefendedPlaces(language string) ([]structs.Place, error)
	RegisterNewUser(nickname string, deviceID string, age string) (int, error)
	StatsHighscores(userID int, time string, mode string) (structs.PlayersStats, error)
	GamesPlayedByTheUser(userID int) ([]structs.Game, error)
	UserReadingTime(deviceID string, culturSite int, time int) error
	RegisterNewGame(deviceID string, game structs.Game) error
	GeneralStatsPlayers() (int, error)
	GeneralStatsPlayedGames() (int, error)
	GeneralStatsDefeatedEnemies() (int, error)
	GeneralStatsSavedPlaces() (int, error)
	GeneralStatsOnSitePlayer(timeWhere string) (int, error)
	GeneralStatsOnlinePlayer(timeWhere string) (int, error)
	GeneralStatsDefeatedEnemiesTimed(timeWhere string) (int, error)
	GeneralStatsSavedPlacesTimed(timeWhere string) (int, error)
	GeneralStatsInvadedPlacesTimed(timeWhere string) (int, error)
	EvaluateTimedStrings() (int, string, string, error)
	MostDefendedPlacesEmpty(language string) []structs.Place

	Ping() error
}

type appdbimpl struct {
	c *sqlx.DB
}

// New returns a new instance of AppDatabase. `db` is the connection, and it's required
func New(db *sqlx.DB) (AppDatabase, error) {
	if db == nil {
		return nil, errors.New("rw database is required when building a AppDatabase")
	}
	return &appdbimpl{
		c: db,
	}, nil
}

func (db *appdbimpl) Ping() error {
	return db.c.Ping()
}

var (
	ErrNotFound = errors.New("item not found")
)

var (
	ErrDuplicate = errors.New("entry already exists")
)

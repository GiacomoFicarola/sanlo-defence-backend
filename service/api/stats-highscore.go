package api

import (
	"encoding/json"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"net/http"
	"strconv"
)

func (rt *_router) statsHighscores(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.URL.Query().Get("userID"))
	if err != nil {
		http.Error(w, "Error in userID", http.StatusBadRequest)
		return
	}

	if !validationmethods.IsValidMode(r.URL.Query().Get("mode")) {
		http.Error(w, "Error in mode value", http.StatusBadRequest)
		return
	}

	if !validationmethods.IsValidTime(r.URL.Query().Get("time")) {
		http.Error(w, "Error in time value", http.StatusBadRequest)
		return
	}

	var result structs.PlayersStats
	result, err = rt.db.StatsHighscores(userID, r.URL.Query().Get("time"), r.URL.Query().Get("mode"))
	result.Mode = r.URL.Query().Get("mode")

	if errors.Is(err, database.ErrNotFound) {
		http.Error(w, "Can't found any data", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, "Can't calculate result", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(result)
}

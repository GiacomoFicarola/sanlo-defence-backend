package database

func (db *appdbimpl) GeneralStatsPlayedGames() (int, error) {
	var i = 0
	err := db.c.Get(&i, "SELECT COALESCE(COUNT(*),0) FROM Game")

	if err != nil {
		return i, err
	}

	return i, nil
}

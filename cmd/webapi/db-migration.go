package main

import (
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/db"
	"github.com/amacneil/dbmate/pkg/dbmate"
	_ "github.com/amacneil/dbmate/pkg/driver/mysql"
	"github.com/sirupsen/logrus"
	"net/url"
	"strings"
)

// writerLogger is a writer wrapper for logrus logger. It allows dbmate to write into the log instead of writing to
// stdout
type writerLogger struct {
	logger logrus.FieldLogger
}

// Write sends the byte buffer to the logrus logger (info level)
func (w *writerLogger) Write(buf []byte) (int, error) {
	w.logger.Info(strings.Trim(string(buf), " \n\r"))
	return len(buf), nil
}

// migrateDB uses dbmate and embedded migration files to upgrade the database to the latest schema (and, optionally
// data) version. If the database does not exist, dbmate will try to create it. If the user has no privilege for
// creating the database or applying DDL changes it returns an error. If the migration fails, it returns an error.
func migrateDB(logger logrus.FieldLogger, databaseDSN *url.URL) error {
	dbm := dbmate.New(databaseDSN)
	dbm.AutoDumpSchema = false
	dbm.WaitBefore = true
	dbm.Log = &writerLogger{logger}
	dbm.FS = db.Migrations
	dbm.MigrationsDir = "migrations"
	dbm.Verbose = true

	return dbm.CreateAndMigrate()
}

/*
Package metrics is a toolkit of functions used to help applying Prometheus
metrics inside the API server. It provide also an HTTP handler to manage
/metrics URL requests.
*/
package metrics

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"time"
)

var (
	metricsReqProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "requests_total",
		Help: "The total number of compat API requests served by the instance",
	})

	metricsReqDelay = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "requests_delay",
		Help:    "The delay of each compat API request",
		Buckets: prometheus.ExponentialBuckets(5, 4, 6),
	})

	metricsReplyCode = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "http_responses",
		Help: "The number of HTTP responses",
	}, []string{"code"})

	metricsAPIDelay = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "requests_api_delay",
		Help:    "The delay of each API request divided by request",
		Buckets: prometheus.ExponentialBuckets(5, 4, 6),
	}, []string{"httpapi"})
)

// MeasureServer is a middleware for HTTP requests measurements (status codes,
// delays)
func MeasureServer(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI == "/metrics" ||
			r.RequestURI == "/version" ||
			r.RequestURI == "/liveness" {
			// Bypass for internal endpoints
			h.ServeHTTP(w, r)
			return
		}

		startts := time.Now()
		var sw http.ResponseWriter
		if _, ok := w.(http.Flusher); ok {
			sw = &StatusCollectorWriterFlusher{StatusCollectorWriter{ResponseWriter: w}}
		} else {
			sw = &StatusCollectorWriter{ResponseWriter: w}
		}
		h.ServeHTTP(sw, r)

		metricsReqProcessed.Inc()
		metricsReqDelay.Observe(float64(time.Since(startts) / time.Millisecond))
		if f, ok := sw.(*StatusCollectorWriter); ok {
			metricsReplyCode.WithLabelValues(fmt.Sprintf("%d", f.status)).Inc()
		} else if f, ok := sw.(*StatusCollectorWriterFlusher); ok {
			metricsReplyCode.WithLabelValues(fmt.Sprintf("%d", f.status)).Inc()
		}
	})
}

// Measure measures the time elapsed for a given HTTP handler
func Measure(name string, handler httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		startts := time.Now()
		handler(w, r, ps)
		metricsAPIDelay.WithLabelValues(name).Observe(float64(time.Since(startts) / time.Millisecond))
	}
}

// Handler returns the Prometheus HTTP handler for metrics output
func Handler() http.Handler {
	return promhttp.Handler()
}

package database

import "fmt"

func (db *appdbimpl) UserDelete(deviceID string) error {
	userID := -1
	err := db.c.Get(&userID, "SELECT identifier FROM User WHERE device_ID = ?", deviceID)

	if userID < 0 || err != nil {
		return ErrNotFound
	}
	newNickName := fmt.Sprint("User deleted ", userID)
	_, err = db.c.Exec("UPDATE User SET nickname = ?, device_ID = ?, deleted = true WHERE device_ID = ?", newNickName, userID, deviceID)
	return err
}

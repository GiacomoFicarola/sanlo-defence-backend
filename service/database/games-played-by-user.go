package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) GamesPlayedByTheUser(userID int) ([]structs.Game, error) {
	var games []structs.Game

	err := db.c.Select(&games, `
	  SELECT g.identifier, g.start_time, g.end_time, g.place,
	      g.victory, g.wave_reached, g.defeated_enemies, g.damage_dealt, g.damage_received,
	      g.result, g.geolocalized, g.count_power_ups, g.hard_mode
	  FROM Game g
	  WHERE g.user = ?
	  GROUP BY g.identifier`, userID)

	for i := 0; i < len(games); i++ {
		err = db.c.Select(&games[i].BuiltTowers, `
		SELECT b.tower AS id, t.name AS name, b.avg_damage AS avg_damage, b.count AS count
		FROM Built b
		JOIN Tower t ON b.tower = t.identifier
		WHERE b.game = ?
		`, games[i].Identifier)
	}

	for i := 0; i < len(games); i++ {
		err = db.c.Select(&games[i].SpawnedEnemies, `
		SELECT s.enemy AS id, e.name AS name, s.avg_damage AS avg_damage, s.count AS count
		FROM Spawned s
		JOIN Enemy e ON s.enemy = e.identifier
		WHERE s.game = ?
		`, games[i].Identifier)
	}

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return games, err
	}

	if len(games) == 0 {
		return games, ErrNotFound
	}

	return games, nil
}

-- migrate:up
CREATE TABLE IF NOT EXISTS Built
(
    game INT,
    tower INT,
    avg_damage INT,
    count INT,
    FOREIGN KEY (game) REFERENCES Game(identifier),
    FOREIGN KEY (tower) REFERENCES Tower(identifier)
);

-- migrate:down
DROP TABLE IF EXISTS Built;

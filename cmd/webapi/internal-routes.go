package main

import (
	"git.sapienzaapps.it/sapienzaapps/go-api-server-libraries/metrics"
	"net/http"
)

// versionHandler is an HTTP handler that returns the app (build) version and build date
func versionHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	_, _ = w.Write([]byte(AppVersion + " - " + BuildDate))
}

// internalRoutesHandler is an HTTP middleware that handles internal routes, like metrics or version handlers. If no
// internal handler is present for the requested path, the request is passed to the next handler (the parameter).
func internalRoutesHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {
			if r.RequestURI == "/metrics" {
				metrics.Handler().ServeHTTP(w, r)
				return
			} else if r.RequestURI == "/version" {
				http.HandlerFunc(versionHandler).ServeHTTP(w, r)
				return
			}
		}
		h.ServeHTTP(w, r)
	})
}

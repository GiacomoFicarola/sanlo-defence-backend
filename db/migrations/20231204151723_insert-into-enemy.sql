-- migrate:up
CREATE PROCEDURE IF NOT EXISTS insertenemyifnotexists(IN enemy VARCHAR(128))
BEGIN
    DECLARE enemy_count INT;

SELECT COUNT(*) INTO enemy_count FROM Enemy WHERE name = enemy;

IF enemy_count = 0 THEN
        INSERT INTO Enemy (name) VALUES (Enemy);
END IF;
END;

CALL insertenemyifnotexists('teppista');
CALL insertenemyifnotexists('molesto');
CALL insertenemyifnotexists('nostalgico');
CALL insertenemyifnotexists('palazzinaro');
CALL insertenemyifnotexists('creditore');
CALL insertenemyifnotexists('capofamiglia');

-- migrate:down
CREATE PROCEDURE IF NOT EXISTS removeenemyifnotexists(IN enemy VARCHAR(128))
BEGIN
    IF EXISTS (SELECT * FROM Enemy WHERE name = enemy) THEN
DELETE FROM Enemy WHERE name = enemy;
END IF;
END;

CALL removeenemyifnotexists('teppista');
CALL removeenemyifnotexists('molesto');
CALL removeenemyifnotexists('nostalgico');
CALL removeenemyifnotexists('palazzinaro');
CALL removeenemyifnotexists('creditore');
CALL removeenemyifnotexists('capofamiglia');

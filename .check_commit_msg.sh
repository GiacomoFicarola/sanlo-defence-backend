#!/usr/bin/env bash
set -euo pipefail

STARTING_BRANCH="${1-}"
CURRENT_BRANCH="${2-}"

if [ "$STARTING_BRANCH" == "$CURRENT_BRANCH" ]; then
	exit 0
fi

commitfail() {
	echo "Invalid commit message in the history: \"$1\". To fix, please read here: https://github.com/RomuloOliveira/commit-messages-guide"
	exit 1
}

cleanup() {
	unset IFS
}
trap cleanup EXIT
IFS=$'\n'

BLACKLIST_LITERAL=(
	"codecheck edits"
	"update postman collection"
	"update postman-collection"
	"aggiunge modifiche per il codecheck"
	"fix minor bugs"
	"update doc"
	"fix"
	"fixed"
	"bugfix"
	"bug fix"
	"bug fixed"
	"bug fixes"
	"some fix"
	"some fixes"
	"solved a bug"
	"risolti alcuni bug"
	"resolve bugs"
	"refactoring"
	"solved"
	"bug solved"
	"string fix"
	"fix changes"
	"update strings.xml"
	"update schema.sql"
	"update db/schema.sql"
	"solved a minor bug"
	"risolvi errori codecheck"
	"minor changes"
	"minor fixes"
	"minor ui fixes"
	"minor ui changes"
	"minor bugfix"
	"fix ci/cd hangs"
	"fix typo"
	"fixed code check"
	"update"
	"test"
)

BLACKLIST_RX=(
	"^changes for pipeline[ #0-9]+"
	"^fixed"
	"^[a-z]+ed"
)

MESSAGES=$(git log --oneline --pretty=format:%s "${STARTING_BRANCH}..${CURRENT_BRANCH}" | tr '[:upper:]' '[:lower:]')

for MSG in $MESSAGES; do
#	if [[ "$MSG" != *" "* ]]; then
#		commitfail "$MSG"
#	fi

	for BLMSG in "${BLACKLIST_LITERAL[@]}"; do
		if [ "$MSG" == "$BLMSG" ]; then
			commitfail "$MSG";
		fi
	done

	for BLMSG in "${BLACKLIST_RX[@]}"; do
		if [[ "$MSG" =~ $BLMSG ]]; then
			commitfail "$MSG";
		fi
	done
done

#DUPLINES=$(git log --oneline --pretty=format:%s "${STARTING_BRANCH}..${CURRENT_BRANCH}" | tr '[:upper:]' '[:lower:]' | grep -vE '^$' | sort | uniq -d | wc -l)
#if [ "$DUPLINES" -gt 0 ]; then
#	echo "Duplicated commit messages detected. Please modify these commit messages or squash these commits:"
#	git log --oneline --pretty=format:%s "${STARTING_BRANCH}..${CURRENT_BRANCH}" | tr '[:upper:]' '[:lower:]' | grep -vE '^$' | sort | uniq -D
#fi

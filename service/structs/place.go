package structs

type Place struct {
	ID               int    `json:"id,omitempty" db:"identifier"`
	Name             string `json:"name,omitempty" db:"name"`
	Description      string `json:"description,omitempty" db:"description"`
	IsSpecial        bool   `json:"is_special" db:"is_special"`                         // Indicate whether the place of culture is special or not
	URLImg           string `json:"url_img,omitempty" db:"url_img"`                     // Indicate the url to the image of the place of culture
	Address          string `json:"address,omitempty" db:"address"`                     // address or gps coordinates of the place
	LevelCoordinates string `json:"level_coordinates,omitempty" db:"level_coordinates"` // level id and x,y coordinates for the place icon
	NumGames         int    `json:"num_games,omitempty" db:"num_games"`
}

package database

func (db *appdbimpl) GeneralStatsPlayers() (int, error) {
	var i = 0
	err := db.c.Get(&i, "SELECT COALESCE(COUNT(*),0) FROM User WHERE deleted = false;")

	if err != nil {
		return i, err
	}

	return i, nil
}

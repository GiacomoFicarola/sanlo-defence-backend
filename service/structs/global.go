package structs

type Global struct {
	Players         int `json:"players,omitempty"`
	PlayedGames     int `json:"played_games,omitempty"`
	DefeatedEnemies int `json:"defeated_enemies,omitempty"`
	SavedPlaces     int `json:"saved_places,omitempty"`
}

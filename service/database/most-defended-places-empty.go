package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) MostDefendedPlacesEmpty(language string) []structs.Place {
	var places []structs.Place
	var err error
	if language == "ita" {
		err = db.c.Select(&places, `
		SELECT p.identifier, p.name, p.description, p.is_special,
			   p.url_img, p.address, p.level_coordinates
		FROM Place p
		WHERE p.is_special = false
		LIMIT 3;`)
	} else {
		err = db.c.Select(&places, `
		SELECT p.identifier, p.name, p.description, p.is_special,
			   p.url_img, p.address, p.level_coordinates
		FROM Place p
		JOIN PlaceLocalization pl ON pl.place = p.identifier
		JOIN Language l ON pl.language = l.identifier
		WHERE p.is_special = false AND l.name = ?
		LIMIT 3;`, language)
	}

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return places
	}

	if len(places) == 0 {
		return places
	}
	return places
}

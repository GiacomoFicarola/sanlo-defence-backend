package structs

type ViewsTime struct {
	Time  int `json:"time,omitempty" db:"time"`
	Views int `json:"views,omitempty" db:"views"`
}

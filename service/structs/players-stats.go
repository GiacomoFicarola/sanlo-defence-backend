package structs

type PlayersStats struct {
	PlayerRank PlayerStats   `json:"playerRank,omitempty"`
	Mode       string        `json:"mode,omitempty"` // indicate if \"expert\" or \"novice\"
	Time       string        `json:"time,omitempty"` // Indicate if \"alltime\",\"monthly\",\"weekly\",\"daily\"
	Ranks      []PlayerStats `json:"ranks,omitempty"`
}

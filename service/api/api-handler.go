package api

import (
	"net/http"

	"go.opentelemetry.io/otel/trace"
)

// Handler returns an instance of httprouter.Router that handle APIs registered here
func (rt *_router) Handler() http.Handler {
	// Register routes
	rt.registerHandle(http.MethodGet, "/user/exist", rt.wrap(rt.checkNicknameExists))
	rt.registerHandle(http.MethodDelete, "/user", rt.wrap(rt.deleteUser))
	rt.registerHandle(http.MethodPost, "/user", rt.wrap(rt.registerNewUser))
	rt.registerHandle(http.MethodGet, "/users", rt.wrap(rt.listUsers))
	rt.registerHandle(http.MethodGet, "/user/games", rt.wrap(rt.gamesPlayedByUser))
	rt.registerHandle(http.MethodPost, "/user/readingTime", rt.wrap(rt.userReadingTime))
	rt.registerHandle(http.MethodPost, "/game", rt.wrap(rt.registerNewGame))
	rt.registerHandle(http.MethodGet, "/stats/general", rt.wrap(rt.generalStats))
	rt.registerHandle(http.MethodGet, "/stats/mdp", rt.wrap(rt.mostDefendedPlace))
	rt.registerHandle(http.MethodGet, "/data/cultureSites", rt.wrap(rt.dataCultureSite))
	rt.registerHandle(http.MethodGet, "/stats/highscores", rt.wrap(rt.statsHighscores))

	rt.router.ServeFiles("/assets/*filepath", http.Dir(rt.assetsPath))

	// Special routes
	rt.registerHandle(http.MethodGet, "/liveness", rt.liveness)
	return rt.router
}

func (rt *_router) registerHandle(method string, path string, hld http.HandlerFunc) {
	rt.router.HandlerFunc(method, path, func(w http.ResponseWriter, r *http.Request) {
		// Set the pattern as name of the current span
		origspan := trace.SpanFromContext(r.Context())
		origspan.SetName(path)
		hld(w, r)
	})
}

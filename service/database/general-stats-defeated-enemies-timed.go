package database

func (db *appdbimpl) GeneralStatsDefeatedEnemiesTimed(timeWhere string) (int, error) {
	var i = 0
	err := db.c.Get(&i, `
		SELECT COALESCE(SUM(defeated_enemies),0) AS total_defeated_enemies
		FROM Game g
		WHERE `+timeWhere+` ;`)

	if err != nil {
		return i, err
	}

	return i, nil
}

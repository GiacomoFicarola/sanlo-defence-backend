-- migrate:up
CREATE TABLE IF NOT EXISTS User
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nickname VARCHAR(256) UNIQUE NOT NULL,
    device_ID VARCHAR(256) UNIQUE NOT NULL,
    age INT,
    registration_time TIMESTAMP,
	deleted BOOLEAN
);

-- migrate:down
DROP TABLE IF EXISTS User;

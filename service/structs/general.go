package structs

type General struct {
	Timed  Timed  `json:"timed,omitempty"`
	Global Global `json:"global,omitempty"`
}

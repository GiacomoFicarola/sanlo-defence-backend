-- migrate:up
CREATE TABLE IF NOT EXISTS PlaceLocalization
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(256),
    description VARCHAR(256),
	place INT,
	language INT,
    FOREIGN KEY (place) REFERENCES Place(identifier),
	FOREIGN KEY (language) REFERENCES Language(identifier)
);

-- migrate:down
DROP TABLE IF EXISTS PlaceLocalization;

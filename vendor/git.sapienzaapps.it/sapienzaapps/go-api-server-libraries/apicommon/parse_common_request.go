package apicommon

import (
	"fmt"
	"github.com/Masterminds/semver"
	"net/http"
	"strconv"
)

// AppInfo is the common set of data that SapienzaApps applications sends in
// each request (used to detect which version of API we should provide)
type AppInfo struct {
	// from x-app-build header
	Build int64

	// from x-app-version header, as string
	VersionString string

	// from x-app-version header, decoded as semantic version
	Version *semver.Version

	// from x-app-platform header, unparsed
	Platform string

	// from x-app-lang header, unparsed
	Lang string

	// from x-app-package header, unparsed
	Package string
}

// ParseAppInfoHeaders read SapienzaApps headers and parse them into a
// structure. Note that if fields are mandatory, you need to check for
// default values (empty, zeros, etc)
func ParseAppInfoHeaders(r *http.Request) (AppInfo, error) {
	ret := AppInfo{}
	var err error

	if r.Header.Get("x-app-build") == "" {
		// Skip parse
	} else {
		ret.Build, err = strconv.ParseInt(r.Header.Get("x-app-build"), 10, 64)
		if err != nil {
			return ret, fmt.Errorf("can't parse app build: %w", err)
		}
	}

	ret.VersionString = r.Header.Get("x-app-version")

	if r.Header.Get("x-app-version") == "" {
		// Skip parse
	} else {
		ret.Version, err = semver.NewVersion(ret.VersionString)
		if err != nil {
			return ret, fmt.Errorf("can't parse app version: %w", err)
		}
	}

	ret.Lang = r.Header.Get("x-app-lang")
	// TODO: check language format

	ret.Platform = r.Header.Get("x-app-platform")
	// TODO: check platform string format

	ret.Package = r.Header.Get("x-app-package")
	// TODO: check platform string format

	return ret, nil
}

package api

import (
	"encoding/json"
	"errors"
	validationmethods "git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"net/http"

	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/database"
)

func (rt *_router) dataCultureSite(w http.ResponseWriter, r *http.Request) {
	if !validationmethods.IsValidLanguage(r.URL.Query().Get("language")) {
		http.Error(w, "Invalid language value", http.StatusBadRequest)
		return
	}

	places, err := rt.db.DataCultureSite(r.URL.Query().Get("language"))
	if errors.Is(err, database.ErrNotFound) {
		http.Error(w, "Places not calculable", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, "Language not exists", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(places)
}

package validationmethods

import (
	"github.com/gofrs/uuid"
	"regexp"
)

func IsValidMode(mode string) bool {
	if mode == "novice" || mode == "expert" {
		return true
	}
	return false
}

func IsValidTime(time string) bool {
	if time == "allTime" || time == "shortTime" {
		return true
	}
	return false
}

func IsValidUUIDStandard(u string) bool {
	_, err := uuid.FromString(u)
	return err == nil
}

func IsValidUUIDUnityEditor(u string) bool {
	r := regexp.MustCompile(`^[a-f0-9]{40}$`)
	return r.MatchString(u)
}

func IsValidUUID(u string) bool {
	return IsValidUUIDStandard(u) || IsValidUUIDUnityEditor(u)
}

func IsValidLanguage(u string) bool {
	r := regexp.MustCompile(`^[a-zA-Z]{3}$`)
	return r.MatchString(u)
}

func IsValidNickname(u string) bool {
	r := regexp.MustCompile(`^[A-Za-z0-9_]{4,16}`)
	return r.MatchString(u)
}

package api

import (
	"encoding/json"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
	"net/http"
)

func (rt *_router) generalStats(w http.ResponseWriter, _ *http.Request) {
	var general structs.General
	var global structs.Global
	var timed structs.Timed
	var err error

	global.PlayedGames, err = rt.db.GeneralStatsPlayedGames()

	if err != nil {
		http.Error(w, "Error get played games value", http.StatusInternalServerError)
		return
	}

	global.Players, err = rt.db.GeneralStatsPlayers()

	if err != nil {
		http.Error(w, "Error get players value", http.StatusInternalServerError)
		return
	}

	global.DefeatedEnemies, err = rt.db.GeneralStatsDefeatedEnemies()

	if err != nil {
		http.Error(w, "Error get defeated enemies value", http.StatusInternalServerError)
		return
	}

	global.SavedPlaces, err = rt.db.GeneralStatsSavedPlaces()

	if err != nil {
		http.Error(w, "Error get saved places value", http.StatusInternalServerError)
		return
	}

	var numGames, timeName, timeWhere, errEvaluate = rt.db.EvaluateTimedStrings()

	if errEvaluate != nil {
		http.Error(w, "Error evaluating timed strings", http.StatusInternalServerError)
		return
	}
	timed.TimeLapse = timeName

	timed.DefeatedEnemies, err = rt.db.GeneralStatsDefeatedEnemiesTimed(timeWhere)

	if err != nil {
		http.Error(w, "Error get defeated enemies value", http.StatusInternalServerError)
		return
	}

	timed.SavedPlaces, err = rt.db.GeneralStatsSavedPlacesTimed(timeWhere)

	if err != nil {
		http.Error(w, "Error get saved places value", http.StatusInternalServerError)
		return
	}

	timed.InvadedPlaces = numGames - timed.SavedPlaces

	timed.OnlinePlayers, err = rt.db.GeneralStatsOnlinePlayer(timeWhere)

	if err != nil {
		http.Error(w, "Error get online players value", http.StatusInternalServerError)
		return
	}

	timed.OnSitePlayers, err = rt.db.GeneralStatsOnSitePlayer(timeWhere)

	if err != nil {
		http.Error(w, "Error get on site players value", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	general.Global = global
	general.Timed = timed
	_ = json.NewEncoder(w).Encode(general)
}

package database

func (db *appdbimpl) GeneralStatsOnlinePlayer(timeWhere string) (int, error) {
	var i = 0
	err := db.c.Get(&i, `
		SELECT COALESCE(COUNT(DISTINCT u.identifier),0) AS num_users
		FROM User u
		    JOIN Game g ON u.identifier = g.user
		WHERE `+timeWhere+` ;`)

	if err != nil {
		return i, err
	}

	return i, nil
}

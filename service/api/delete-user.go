package api

import (
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/api/validation-methods"
	"net/http"
)

func (rt *_router) deleteUser(w http.ResponseWriter, r *http.Request) {
	if !validationmethods.IsValidUUID(r.Header.Get("deviceID")) {
		http.Error(w, "Invalid deviceID value", http.StatusBadRequest)
		return
	}

	err := rt.db.UserDelete(r.Header.Get("deviceID"))

	if err != nil {
		http.Error(w, "DeviceID not exists", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

**When cloning this repository for a new project, please read the "How to set up a new project from this template" section.**

# Fantastic coffee

## Project structure

* `cmd/` contains all executables; Go programs here should only do "executable-stuff", like reading options from the CLI/env, etc.
	* `cmd/cronjobs` is an example executable for a pure-Go cron daemon; useful when no cron daemons are available (e.g., Docker `FROM scratch` images)
	* `cmd/healthcheck` is an example of a daemon for checking the health of servers daemons; useful when the hypervisor is not providing HTTP readiness/liveness probes (e.g., Docker engine)
	* `cmd/webapi` contains an example of a web API server daemon
* `db/` is the directory for database migrations (use `dbmate` for handling them)
* `demo/` contains some files for a demo/dev environment
* `doc/` contains the documentation (usually, for APIs, this means an OpenAPI file)
* `service/` has all packages for implementing project-specific functionalities
	* `service/api` contains an example of an API server
	* `service/database` contains an example of a database abstraction package
	* `service/globaltime` contains a wrapper package for `time.Time` (useful in unit testing)
* `vendor/` is managed by Go, and contains a copy of all dependencies
* `webui/` is an example of a web frontend in Vue.js; it includes:
	* Bootstrap JavaScript framework
	* a customized version of "Bootstrap dashboard" template
	* feather icons as SVG
	* Go code for release embedding

Other project files include:
* `.gitlab-ci.yml` is an example of a pipeline for GitLab CI
* `Dockerfile` contains specs for building OCI containers
* `Makefile` contains some useful targets:
	* `docker` creates the OCI container
	* `up-deps` starts all dependencies for local development/demo using `docker-compose`
	* `down` destroys all containers started using `up-deps`
	* `test` checks the OpenAPI specification using linters, runs some Go linters and tests, and prints a summary of all outdated dependencies
	* `install-dev-deps` which installs all dependencies for `test` target
	* `doc` for local OpenAPI documentation (will be available at http://127.0.0.1:9000 )
	* `godoc` for code documentation (will be available at http://127.0.0.1:6060/pkg/git.sapienzaapps.it/fantasticcoffee/fantastic-apis/ )
    * `keycloak-export` saves the current (local) Keycloak configuration in the repository
    * `dep-update` updates all Go dependencies in the project
* `open-npm.sh` starts a new (temporary) container using `node:lts` image for safe web frontend development (you don't want to use `npm` in your system, do you?)

## Go vendoring

This project uses [Go Vendoring](https://go.dev/ref/mod#vendoring). You must use `go mod vendor` after changing some dependency (`go get` or `go mod tidy`) and add all files under `vendor/` directory in your commit.

For more information about vendoring:

* https://go.dev/ref/mod#vendoring
* https://www.ardanlabs.com/blog/2020/04/modules-06-vendoring.html

## Node/NPM vendoring

This repository contains the `webui/node_modules` directory with all dependencies for Vue.JS. You should commit the content of that directory and both `package.json` and `package-lock.json`.

## How to set up a new project from this template

You need to:

* Change the Go module path to your module path in `go.mod`, `go.sum`, and in `*.go` files around the project
* Change the database URL inside `demo/config.yml` and database name inside `demo/docker-compose.yml`.
* If no database is needed, remove the `db/` folder along with the database container specification in `demo/docker-compose.yml`, the Go package `service/database` and the references in the `demo/config.yml`
* If no Keycloak is needed:
  * remove the `demo/keycloak` folder
  * remove the keycloak container specification in `demo/docker-compose.yml`
  * remove the Go code referencing the OAuth2/OIDC/Keycloak in:
    * `cmd/webapi/load-configuration.go`
    * `cmd/webapi/main.go`
	* `service/api/api.go`
    * `service/api/api-context-wrapper.go` (the whole `wrapAuth` method)
* Rewrite the API documentation `doc/api.yaml`
* If no web frontend is expected, remove `webui` directory, the file `cmd/webapi/register-webui.go` and the frontend build container and COPY commands in the `Dockerfile`.
* If no cronjobs or health checks are needed, remove them from `cmd/`
* Update top/package comment inside `cmd/webapi/main.go` to reflect the actual project usage, goal, and general info
* Update the code in `run()` function (`cmd/webapi/main.go`) to connect to databases or external resources
* Write API code inside `service/api`, and create any further package inside `service/` (or subdirectories)

## Keycloak

If keycloak is used, the local version of Keycloak is started when `make up-deps` is launched, and it's available at http://127.0.0.1:8060/ (default credentials are in the `demo/docker-compose.yml`).

Users can be created by opening the admin panel and switching to the realm (default `demo-dev`). The `demo-dev` realm is preloaded with a default user `jdoe` (`john@doe.com`) password `jdoe`.

To authenticate against Keycloak when using the OpenAPI / Swagger Editor:
* Launch `make doc` and open the Swagger UI at http://127.0.0.1:9000/
* Click on "Authorize"
* Scroll down to the `OpenID (OAuth2, password)` - if there are multiple sections with the same name, use the one referring to the same URL that is present in `demo/config.yml`
* fill the form with a valid username and password previously configured in the `demo-dev` realm. use `openapi-client` as Client ID. Leave the Client Secret field empty.
* select `openid`, `profile` and `email` scopes and click on "Authorize"

## How to build

### Docker

```shell
make docker
```

### Without docker/make

If you're not using the WebUI, or if you don't want to embed the WebUI into the final executable, then:

```shell
go build ./cmd/webapi/
```

If you're using the WebUI and you want to embed it into the final executable:

```shell
./open-npm.sh
# (inside the NPM container)
npm run build-embed
exit
# (outside the NPM container)
go build -tags webui ./cmd/webapi/
```

## How to run (in development mode)

You can launch the backend only using:

```shell
go run ./cmd/webapi/
```

If you want to launch the WebUI, open a new tab and launch:

```shell
./open-npm.sh
# (inside the NPM container)
npm run dev
```

## Known issues

### Apple ARM64 / Linux ARM64

Node does not support mixed architectures in `node_modules`. If you want to build/run the environment in non Intel-compatible architectures you need to use the `open-npm.sh` script.

If you're using Linux, make sure that your `qemu-user-static` version is at least 7.1. On Debian Bullseye, you can use the version in backports:

```sh
$ sudo apt-get install -t bullseye-backports qemu-user-static
```

### My build works when I use `npm run dev`, however there is a Javascript crash in production

Some errors in the code are somehow not shown in `vite` development mode. To preview the code that will be used in production settings, use the following commands:

```shell
./open-npm.sh
# (here you're inside the NPM container)
npm run build-prod
npm run preview
```

## License

See [LICENSE](LICENSE).

## Required tools (for development)

This code is compatible with GNU/Linux and macOS.

* Git
* Go
* GNU make (other makes may work)
* Docker (engine) >= 23
* `godoc` (for code documentation only): `go install golang.org/x/tools/cmd/godoc`
* [dbmate](https://github.com/amacneil/dbmate/) (for database schema change management only)
* For `make test` command:
	* `go-licenses`: `go install github.com/google/go-licenses@v1.2.1`
	* `golangci-lint`: `go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.48.0`
	* `go-mod-outdated`: `go install github.com/psampaz/go-mod-outdated@v0.8.0`

package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) ListUsers() ([]structs.User, error) {
	var users []structs.User
	err := db.c.Select(&users, "SELECT identifier, nickname, age FROM User")

	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotFound
		}
		return nil, err
	}
	return users, nil
}

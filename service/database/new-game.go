package database

import (
	"database/sql"
	"errors"
	"git.sapienzaapps.it/fantasticcoffee/fantastic-apis/service/structs"
)

func (db *appdbimpl) RegisterNewGame(deviceID string, game structs.Game) error {
	var user int
	err := db.c.Get(&user, "SELECT identifier FROM User WHERE device_ID = ?", deviceID)

	if errors.Is(err, sql.ErrNoRows) {
		return ErrNotFound
	}

	if err != nil {
		return err
	}

	var placedTowers int
	for i := 0; i < len(game.BuiltTowers); i++ {
		placedTowers += game.BuiltTowers[i].Count
	}

	result, err := db.c.Exec(`
		INSERT INTO Game (start_time,end_time, hard_mode, victory, wave_reached, placed_towers,
                  defeated_enemies, damage_dealt, damage_received, result, geolocalized, count_power_ups,
                  place, user)
			VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, game.StartTime, game.EndTime, game.HardMode, game.Victory, game.WaveReached, placedTowers, game.DefeatedEnemies, game.DamageDealt, game.DamageReceived, game.Result, game.Geolocalized, game.CountPowerUps, game.CultureSite, user)

	var id64, _ = result.LastInsertId()
	var id int = int(id64)

	for i := 0; i < len(game.BuiltTowers); i++ {
		_, err = db.c.Exec(`INSERT INTO Built (tower, game, avg_damage, count)
			VALUES (?,?,?,?)`, game.BuiltTowers[i].ID, id, game.BuiltTowers[i].AvgDamage, game.BuiltTowers[i].Count)
	}

	for i := 0; i < len(game.SpawnedEnemies); i++ {
		_, err = db.c.Exec(`INSERT INTO Spawned (enemy, game, avg_damage, count)
			VALUES (?,?,?,?)`, game.SpawnedEnemies[i].ID, id, game.SpawnedEnemies[i].AvgDamage, game.SpawnedEnemies[i].Count)
	}

	if err != nil {
		return err
	}
	return nil
}

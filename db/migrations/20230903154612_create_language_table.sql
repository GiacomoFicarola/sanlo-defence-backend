-- migrate:up
CREATE TABLE IF NOT EXISTS Language
(
    identifier INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(256)
);

-- migrate:down
DROP TABLE IF EXISTS Language;
